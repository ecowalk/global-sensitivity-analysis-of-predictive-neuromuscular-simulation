# -*- coding: utf-8 -*-
"""
********************* Module scone_batch_workflow.py **************************

scone_batch_workflow.py provides a set of functions to set up, optimize and 
analyse multiple .scone scenarios at once using batch scripts (see [1]). 

The module includes the following functions: 
    
    - delete_files(directory:str, file_extension:str)
    - clear_folder(directory:str)
    
    - generate_scone_files(
        input_file:str, 
        output_file:str, 
        factors:list[float], 
        file_no:str, 
        var_naming:list[str]
      )
    - generate_model_files(
            input_model:str, 
            model_dir:str,
            model_name:str,
            factors:list[float],
            file_no:str, 
            var_naming:list[str]
        )
    
    - generate_scone_bat_eval(folder_path:str, bat_file_name:str)
    - generate_scone_bat_optim(folder_path:str, bat_file_name:str)
    - start_batch_file(dir_path:str, file_name:str)
    
    - remove_intermediate_par_files(dir_path:str)
    
    
NOTE: To use this module, SCONE must be installed on the user's machine and 
      the sconecmd.exe binary must be located in 'C:\Program Files\SCONE\bin'.

###############################################################################
References:

[1] https://scone.software/doku.php?id=doc:batch 
    
###############################################################################

 ------------------------------------------------------------------------------
 
 © Alexandra Buchmann, Chair of Applied Mechanics, alexandra.buchmann@tum.de
 
 ------------------------------------------------------------------------------
 Initiaized:       Fri Apr 21 2023 12:55:07, Munich
 Last modified:    04-01-2024, Munich 
 ------------------------------------------------------------------------------
"""

#%% Clear files and folders
def delete_files(directory:str, file_extension:str):
    print(f"Clear all {file_extension} files in {directory}") # ---------------
    import os
    
    files = os.listdir(directory)
    for file in files:
        if file.endswith(file_extension):
            file_path = os.path.join(directory, file)
            if os.path.isfile(file_path):
                os.remove(file_path)
                
                
def clear_folder(directory:str):
    print(f"Clear all files and subdirectories in {directory}") # -------------
    import os
    
    for root, dirs, files in os.walk(directory): # files 
        for file in files:
            file_path = os.path.join(root, file)
            try:
                os.remove(file_path)
            except OSError as e:
                print(f"Error: {e.strerror}")

    for root, dirs, files in os.walk(directory, topdown=False): # directories 
        for dir in dirs:
            dir_path = os.path.join(root, dir)
            try:
                os.rmdir(dir_path)
            except OSError as e:
                print(f"Error: {e.strerror}")
                

#%% Generate .scone scenarios and .hfd model files
def generate_scone_files(
        input_scenario: str,
        optim_dir: str,
        scenario_name: str,
        factors: list[float],
        file_no: str,
        var_naming: list[str]
    ):
    """
    Generates new SCONE input files by modifying a given SCONE scenario. 
    
    Args:
        input_scenario (str):  Path to the given SCONE scenario to be modified.
        optim_dir (str):       Directory where the SCONE files are stored.
        scenario_name (str):   Full path and name to store new scenarios.
        factors (list[float]): List of numeric factors to be replaced.
        file_no (str):         File number for file ending.
        var_naming list[str]:  List of names of the factors in the .scone 
                               file that will be replaced by the given factors.
                               The list must have regular expression notation!
                               
    Returns:
        None.

    Raises:
        ValueError: If a specified factor is not found in the input file.

    Note: For each scenario a seperate .hfd model file needs to be created 
    following the same file numbering system!
    
    Example:
        >>> generate_scone_files(
                model_execution_config['scenario'],
                model_execution_config['optim_dir'],
                model_execution_config['scenario_name'], 
                X[i,:len(SCONE_labels)], 
                num_list[i], 
                SCONE_labels
            )
    """
    import re 
    import os
    
    with open(input_scenario, "r") as f:
        contents = f.read()
    
    
    for i, var in enumerate(var_naming): # Change var_names values in SCONE file
        pattern = re.compile(var + r'\s*=\s*([\d.]+)')

        def replace_pattern(match):
            return match.group(0).replace(match.group(1), f'{factors[i]}')

        contents, num_replacements = re.subn(pattern, replace_pattern, contents)
        if num_replacements == 0:
            raise ValueError(f"The specified factor '{var}' was not found in the input file.")
    
    
    def replace_pattern(match): # Change model name in entire file
        return match.group().replace("H0914.hfd", f'H0914_{file_no}.hfd')
    
    pattern = r'H0914\.hfd'
    contents = re.sub(pattern, replace_pattern, contents)
    
    
    file_path_write = os.path.join(optim_dir, scenario_name)
    new_file_name = f"{file_path_write[:-6]}_{file_no}.scone" # [:-6] as '.scone' has six characters

    with open(new_file_name, "w") as f:
        f.write(contents)
    

def generate_model_files(
        input_model:str, 
        model_dir:str,
        model_name:str,
        factors:list[float],
        file_no:str, 
        var_naming:list[str]
    ):
    """
    Generates new Hyfydy model based on a given model.

    Args:
        input_model (str):     Path to the given Hyfydy model to be modified.
        model_dir (str):       Directory where the new created model is stored.
        model_name (str):      Name of the new models that are created.
        factors list[float]:   List of numeric factors to be replaced.
        file_no (str):         A file number for file ending.
        var_naming list[str]:  A list of names of the factors in the .scone 
                               file that will be replaced by the given factors.
                               The list must have regular expression notation!

    Returns:
        None.

    Raises:
        ValueError: If a specified factor is not found in the input file.

    Example:
        >>> generate_model_files(model_execution_config['model'],
                                 model_execution_config['model_dir'], 
                                 model_execution_config['model_name'], 
                                 X[i,-len(Model_labels):], 
                                 num_list[i], 
                                 Model_labels)
    """
    import re 
    import os


    with open(input_model, "r") as f:
        contents = f.read()
    
    
    for i, var in enumerate(var_naming): # Change var_names values in Hyfydy file
        pattern = re.compile(var + r'\s*=\s*([+-]?\d+(?:\.\d+)?)')
        
        def replace_float(match):
            old_value = match.group(1)
            # multiply factors by -1 to shift all values is in neg. x-dir ->
            # depends on the way the factors are defined in the Hyfydy file
            new_value = f'{factors[i]*(-1)}' 
            return match.group().replace(old_value, new_value)

        contents, num_replacements = re.subn(pattern, replace_float, contents)
        if num_replacements == 0:
            raise ValueError(f"The specified factor '{var}' was not found in the input file.")
    
    
    file_path_write = os.path.join(model_dir, model_name)
    new_file_name = f"{file_path_write[:-4]}_{file_no}.hfd" # [:-4] as .hfd has four characters

    with open(new_file_name, "w") as f:
        f.write(contents)
    
    
#%% Create and execute .bat files
def generate_scone_bat_optim(folder_path:str, bat_file_name:str, batch_size:int):
    """
    Creates a Windows batch file to optimize all .scone files in folder_path. 

    Args:
        folder_path (str):   Path to the folder containing the .scone files.
        bat_file_name (str): Name of the batch file the will be created.
        batch_size (int):    Number of sconecmd.exe files started in parallel.

    Returns:
        None.

    Raises:
        FileNotFoundError: If the specified folder does not exist.
        OSError: If there is an error creating the batch file.

    NOTE: sconecmd.exe is used in the batch command - important options:
        -o: Specifies the optimization input file.
        -e: Specifies the evaluation input file.

    Example:
        >>> create_scone_bat_optim("C:\\Users\\User\\Documents\\SconeFiles","optimize.bat")
    """
    import os
    
    script_content = '''
    @echo off
    setlocal enabledelayedexpansion
    
    set SCONECMD="C:\\Program Files\\SCONE\\bin\\sconecmd.exe"
    
    set /A files_tot = 0
    set /A batch_tot = 0
    
    set /A file_count = 0
    set /A batch_count = 1
    set /A batch_size = {}
    
    for %%f in (*.scone) do (
		set /a files_tot=!files_tot!+1
	)
	set /a batch_tot=(!files_tot!+!batch_size!-1)/!batch_size!
	echo Total number of optimizations required: !files_tot! ^
    (!batch_tot! batches with max. !batch_size! files each)
    
    for %%f in (*.scone) do (
        start "%%f" %SCONECMD% -o "%%f"
        REM timeout 5 > nul
        
        set /a file_count=!file_count!+1
    
        if !file_count!==!batch_size! (
            call :wait_to_finish
        )
    )
    
    :wait_to_finish
    tasklist /fi "ImageName eq sconecmd.exe" /fo csv 2>NUL | find /I "sconecmd.exe">NUL
    if "%ERRORLEVEL%"=="0" (
        goto wait_to_finish
    ) else (        
        echo - Batch !batch_count! of !batch_tot! completed
        set /a batch_count=!batch_count!+1
        set /a file_count=0
    )
    '''

    script_content = script_content.format(batch_size)
    bat_path = os.path.join(folder_path, bat_file_name)
    
    with open(bat_path, 'w') as file:
        file.write(script_content)
        
    print(f"Batch script generated: '{bat_path}'") # --------------------------
    


def generate_scone_bat_eval(folder_path:str, bat_file_name:str):
    """
    Creates a Windows batch file to evaluates all .par files in folder_path and 
    all its subfolders starting with the subfolder that was created first.

    Args:
        folder_path (str):   Path to the folder containing the .scone files.
        bat_file_name (str): Name of the batch file to create.

    Returns:
        None.

    Raises:
        FileNotFoundError: If the specified folder does not exist.
        OSError: If there is an error creating the batch file.

    Options for sconecmd:
        -o: Specifies the optimization input file.
        -e: Specifies the evaluation input file.

    Example:
        >>> create_scone_bat_eval("C:\\Users\\User\\Documents\\SconeFiles", "eval.bat")
    """
    
    import os
        
    script_content = '''
    @echo off
    setlocal enabledelayedexpansion
    set SCONECMD="C:\\Program Files\\SCONE\\bin\\sconecmd.exe"
    set counter=0
    
    for /r %%f in (*.par) do (
        set /a counter+=1
        set resultFile=results_!counter!.txt
        
        start /B "%%f" %SCONECMD% -e "%%f" > "!resultFile!"
    )
    
    endlocal
    '''
    
    bat_path = os.path.join(folder_path, bat_file_name)
    with open(bat_path, "w") as file:
        file.write(script_content)
        
    print(f"Batch script generated: '{bat_path}'") # --------------------------


def execute_bat_file(dir_path:str, file_name:str):
    """
    Run a .bat (Windows batch) file from Python, displaying the echo in the 
    command window and wait for the file to complete before continuing. 

    Args:
        dir_path (str): Path to the directory where the .bat file is located.
        file_name (str): Name of the .bat file to be executed.

    Returns:
        None

    Raises:
        OSError: If the batch file cannot be started.

    Example:
        >>> start_batch_file("C:/mydir", "myscript.bat")
    """
    
    import subprocess
    process = subprocess.Popen([file_name], shell=True, 
                          cwd=dir_path, stdout=subprocess.PIPE)

    while True: # Print batch echos in Python command line
        line = process.stdout.readline().decode('latin-1').strip()
        if line:
            print(line)
        else:
            break                
    
    stdout, stderr = process.communicate() # Wait for process to finish
    
    
#%% Remove intermediate optimization results    
def remove_intermediate_par_files(dir_path:str):
    """
    Remove all intermediate .par optimization files in the specified directory 
    and only keep the final optimization result.

    Args:
        dir_path (str): The directory path to search for intermediate par files.

    Returns:
        None
        
    Example: 
        >>> remove_intermediate_par_files('../results')
    """
    
    import os
    import glob
    
    for subdir, dirs, files in os.walk(dir_path):
        par_files = glob.glob(os.path.join(subdir, "*.par"))
        if not par_files:
            continue
        newest_par_file = max(par_files, key=os.path.getctime)
        for file in par_files:
            if file != newest_par_file:
                os.remove(file)
    
            


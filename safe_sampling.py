# -*- coding: utf-8 -*-
"""
************************ Module safe_sampling.py ******************************

safe_sampling.py provides a function to sample input parameters for sensitivity 
analysis:
    
sample_inputs(M, N, samp_strat, distr_fun, distr_par, GSA_met, r, design_type)

 ------------------------------------------------------------------------------
 
 © Alexandra Buchmann, Chair of Applied Mechanics, alexandra.buchmann@tum.de
 
 ------------------------------------------------------------------------------
 Initiaized:       Tue Jun 6 17:39:37 2023, Munich
 Last modified:    04-01-2024, Munich 
 ------------------------------------------------------------------------------
"""

def sample_inputs(M, N, samp_strat, distr_fun, distr_par, GSA_met, r, design_type):
    """
    Generate sample inputs for Global Sensitivity Analysis (GSA) based on the 
    specified parameters and chosen GSA method.
    
    Args:
        M (int): Number of input parameters (dimensions).
        N (int): Sample size for GSA methods VBSA and PAWN.
        samp_strat (str): Sampling strategy for AAT and OAT sampling used in 
        VBSA, EET and PAWN methods.
        distr_fun (callable): Distribution function to generate samples from.
        distr_par (tuple or dict): Shape parameters for distribution function.
        GSA_met (str): Global Sensitivity Analysis (GSA) method. Supported 
        options are 'VBSA', 'FAST', 'EET', 'PAWN'.
        
        ------------ For EET method only -----------------------
        r (int): Number of model evaluations for EET method.
        design_type (str): Design type for OAT_sampling used in EET method.
    
    Returns:
        X_all (ndarray): Array containing the generated sample inputs for the GSA method.
    
    Raises:
        ValueError: If the specified GSA method is not supported.
    
    Example:
        >>> X_all = sample_inputs(M, N, samp_strat, distr_fun, distr_par, GSA_met, r, design_type)
    """

    import numpy as np
    
    import safepython.VBSA as VB 
    import safepython.FAST as FAST 
    
    from safepython.sampling import AAT_sampling # VBSA/Sobol and FAST methods
    from safepython.sampling import OAT_sampling # EET method
    
    
    if GSA_met =='VBSA': # Sobol, number of model evaluations N*(M+2)
        
        X = AAT_sampling(samp_strat, M, distr_fun, distr_par, 2*N)
        XA, XB, XC = VB.vbsa_resampling(X)
        X_all = np.row_stack((XA, XB, XC)) 
        
        
    elif GSA_met == 'EET': # Morris EEs, number of model evaluations r*(M+1) 
    
        X_all = OAT_sampling(r, M, distr_fun, distr_par, samp_strat, design_type)
    
        
    elif GSA_met == 'FAST': # FAST, automatic number of model evaluations
    
        X_all, _ = FAST.FAST_sampling(distr_fun, distr_par, M)
    
    
    elif GSA_met == 'PAWN': # PAWN, number of model evaluations N 
    
        X_all = AAT_sampling(samp_strat, M, distr_fun, distr_par, N)
        
    else: 
        raise ValueError(f'''No method called '{GSA_met}' - Supported options 
                                         are 'VBSA', 'FAST', 'EET', 'PAWN''')
    
    return X_all
# -*- coding: utf-8 -*-
"""
******************** Module safe_plotting.py **********************************

safe_plotting.py provides a set of functions to plot the outcome of sensitivity 
analysis performed using the SAFE toolbox [1,2]. The functions in this module 
allow the user to adjust the plots provided by SAFE and creates a combined plot 
with all important measures for each method.  


The module includes the following functions: 
    - scatter3d_with_projections(Y_all_eval, X_all, X_Labels, scalar_output, 
                                 output_var_name, GSA_met)
    
    - plot_VBSA_indices (Si, STi, X_Labels, M, ax=None)
    - plot_VBSA(Si, STi, Sic, STic, Si_m, Si_lb, Si_ub, STi_m, STi_lb, STi_ub,
                Inti_m, Inti_lb, Inti_ub, NN, M, X_Labels, param)
    
    - plot_EE(mi, sigma, mi_m, mi_lb, mi_ub, sigma_m, sigma_lb, sigma_ub, 
              mic, mic_m, mic_lb, mic_ub, rr, M, X_Labels, scalar_output, 
              output_var_name)
    
    - plot_FAST_indices(Si_fast, X_Labels, M, scalar_output, output_var_name, 
                        ax=None)
    
    - plot_PAWN (KS_median_m, KS_median_lb, KS_median_ub, KS_mean_m, KS_mean_lb, 
                 KS_mean_ub, KS_max_m, KS_max_lb, KS_max_ub, KS_median_c_m, 
                 KS_median_c_lb, KS_median_c_ub, KS_mean_c_m, KS_mean_c_lb, 
                 KS_mean_c_ub, KS_max_c_m, KS_max_c_lb, KS_max_c_ub, NN, 
                 X_Labels, scalar_output, output_var_name)
    
    - plot_parallel_coordinates(Y_all_eval, X_all, scalar_output, X_Labels)

###############################################################################
References:

[1] https://github.com/SAFEtoolbox/SAFE-python
[2] https://safetoolbox.github.io/
    
###############################################################################

 ------------------------------------------------------------------------------
 
 © Alexandra Buchmann, Chair of Applied Mechanics, alexandra.buchmann@tum.de
 
 ------------------------------------------------------------------------------
 Initiaized:       Thu Jun  1 10:44:07 2023, Munich
 Last modified:    04-01-2024, Munich 
 ------------------------------------------------------------------------------
"""

#%% Specify uniform properties for all figures
                            # (width  , height    )
fig_size_long_landscape     = (24/2.54, 5.5  /2.54)   # 1x4 grid
fig_size_large_rectangle    = (30/2.54, 4.5*4/2.54)   # 2x3 grid
fig_size_medium_rectangle   = (20/2.54, 4.5*2/2.54)   # 1x2 grid
fig_size_small_rectangle    = (6 /2.54, 4.5  /2.54)   # 1x1 bar plots

def set_latex_font():
    import matplotlib.pyplot as plt
    
    plt.rcParams.update({
        "text.usetex": True,
        "font.family": "serif",
        "font.serif": "Computer Modern",
    })
    
def set_figure_properties():
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    mpl.rcParams.update(mpl.rcParamsDefault)
    mpl.rcParams.update({
    'font.size': 12,
    'text.usetex': True,
    })
    
    plt.rc('font', size=12)
    plt.rc('text', usetex=True)
    
    
#%% Scatter plot 
def scatter3d_with_projections(Y_all_eval, X_all, X_Labels, scalar_output, output_var_name, GSA_met):
    """
    Generate a 3D scatter plot with projections onto XY, XZ, and YZ planes.

    Args:
    - Y_all_eval (array): The evaluation values for color mapping.
    - X_all (array): Input data points in 3D space.
    - X_Labels (list): Labels for the X, Y, and Z axes.
    - scalar_output (str): Name of the scalar output variable for colorbar.
    - output_var_name (str): Name of variable set that is analyzed.
    - GSA_met (str): global sensitivity analysis method for saving file name
        
    Returns:
        none
        
    Note: The function generates, displays and saves the plot to a PDF file in 
          plots/{GSA_met}_scatter_{scalar_output}_{output_var_name}.pdf

    Example:
    scatter3d_with_projections(Y_all_eval, X_all, ["X Label", "Y Label", "Z Label"],
                               "APO_max","stiffness", "VBSA")
    """
    
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FuncFormatter
    from mpl_toolkits.axes_grid1.inset_locator import inset_axes
    import numpy as np
    
    # Set figure properties
    set_figure_properties()
    
    cmap = 'turbo'
    marker_size = 1
    
    # Define colorbar names 
    def colorbar_name(variable_name):
        translation_dict = {
            'APO_max': '$P_\mathrm{max}$',
            'APO_AMP': '$P_\mathrm{amp}$',
            'Step_Vel': '$v_\mathrm{HAT}$',
            'E_Metab': '$E_\mathrm{metab}$',
            'E_CoT': 'CoT',
            'R_RoM_Ank': '$R_\mathrm{ank}$',
            'R_RoM_Kne': '$R_\mathrm{kne}$',
            'R_RoM_Hip': '$R_\mathrm{hip}$',
        }
    
        return translation_dict.get(variable_name, variable_name)
    
    # Function to format tick labels
    def format_ticks(value, pos):
        return "{:.1f}".format(value)
    
    x,y,z = X_all.T
    c = Y_all_eval
    
    fig = plt.figure(figsize=fig_size_long_landscape)
    plt.tight_layout()
    grid = plt.GridSpec(1, 4)

    # 3D Scatter Plot in the center top
    ax_3d = fig.add_subplot(grid[0, 0], projection='3d')
    scatter_3d = ax_3d.scatter(x, y, z, c=c, cmap=cmap, s=marker_size)
    ax_3d.view_init(elev=20, azim=45, roll=0)
    ax_3d.set_xlabel(X_Labels[0], labelpad=-5)
    ax_3d.set_ylabel(X_Labels[1], labelpad=-5)
    ax_3d.set_zlabel(X_Labels[2], labelpad=-7.5)
    xticks = ax_3d.get_xticks()
    ax_3d.set_xticks([min(xticks[1:]), max(xticks[:-1])])
    plt.tick_params(axis='x', which='both', pad=-2)
    yticks = ax_3d.get_yticks()
    ax_3d.set_yticks([min(yticks[1:]), max(yticks[:-1])])
    plt.tick_params(axis='y', which='both', pad=-2)
    zticks = ax_3d.get_zticks()
    ax_3d.set_zticks([min(zticks[1:]), max(zticks[:-1])])
    plt.tick_params(axis='z', which='both', pad=-2)
    plt.title('3D View')
      
    axins = inset_axes(ax_3d,
                       width="15%",  
                       height="100%",
                       loc='center left',#'lower center',
                       borderpad=-6)
    fig.colorbar(scatter_3d, cax=axins, pad=0, orientation="vertical")
    plt.title(colorbar_name(scalar_output), y=-0.25)
    
    # XY projection in the center (bottom)
    ax_xy = fig.add_subplot(grid[0, 1])
    scatter_xy = ax_xy.scatter(x, y, c=c, cmap=cmap, s=marker_size)
    ax_xy.set_xlabel(X_Labels[0], labelpad=0)
    ax_xy.set_ylabel(X_Labels[1], labelpad=0)
    plt.title('XY projection')
    
    # XZ projection on the right (top)
    ax_xz = fig.add_subplot(grid[0, 2])
    scatter_xz = ax_xz.scatter(x, z, c=c, cmap=cmap, s=marker_size)
    ax_xz.set_xlabel(X_Labels[0], labelpad=0)
    ax_xz.set_ylabel(X_Labels[2], labelpad=0)
    plt.title('XZ projection')
    
    # YZ projection on the left (top)
    ax_yz = fig.add_subplot(grid[0, 3]) 
    scatter_yz = ax_yz.scatter(y, z, c=c, cmap=cmap, s=marker_size)
    ax_yz.set_xlabel(X_Labels[1], labelpad=0)
    ax_yz.set_ylabel(X_Labels[2], labelpad=0)
    plt.title('YZ projection')
 
        
    # Calculate the average value and set the threshold
    avg = np.nanmean(c)
    threshold = 50
   
    # Filter out outliers based on the threshold
    lb = avg - (avg * threshold / 100)
    ub = avg + (avg * threshold / 100)
   
    # Apply color limits to all scatter plot
    for scatter_plot in [scatter_3d, scatter_xy, scatter_xz, scatter_yz]:
        scatter_plot.set_clim(lb, ub)
   
    # Apply the formatter to the desired axes
    for ax in [ax_xy, ax_xz, ax_yz, ax_3d]:
        ax.xaxis.set_major_formatter(FuncFormatter(format_ticks))
        ax.yaxis.set_major_formatter(FuncFormatter(format_ticks))
        ax.axhline(y=1, c='black', lw=1, ls='--') #xmin=min(x), xmax=max(x),
        ax.axvline(x=1, c='black', lw=1, ls='--') #ymin=min(y), ymax=max(y), 
        ax.set_aspect('equal', 'box')
        
    ax_3d.zaxis.set_major_formatter(FuncFormatter(format_ticks))
    
    set_latex_font()
    
    # Adjust global plot settings and export figure to PDF
    plt.subplots_adjust(hspace=0, wspace=0.5)
    filename = f"plots/{GSA_met}_scatter_{scalar_output}_{output_var_name}.pdf"
    plt.savefig(filename) # bbox_inches="tight" - causes issues with colorbar
    plt.show()  
        

#%% Plots for variance-based sensitivity analysis (VBSA)
def plot_VBSA_indices (Si, STi, X_Labels, M, ax=None):
    """
    Generate grouped bar plot for Sobol indices from VBSA sensitivity analysis.

    Args:
        Si (array): Sobol indices for the main effects.
        STi (array): Sobol indices for the total effects.
        X_Labels (list): Labels for the input variables.
        M (int): Number of parameters being analysed.
        ax (matplotlib.axes.Axes, optional): Axes object upon which to plot the indices. 
            If not specified, a new figure and axes are created.

    Returns:
        None
        
    Example: 
        >>> plot_VBSA_indices (Si, STi, X_Labels, M, ax1)
    """
    
    import matplotlib.pyplot as plt
    import numpy as np
    from numpy.matlib import repmat
    
    
    # Define plot colors (copied from SAFE toolbox plot functions)
    col = np.array([[228, 26, 28], [55, 126, 184], [77, 175, 74],
                   [152, 78, 163], [255, 127, 0]])/256
    
    A = len(col)
    L = int(np.ceil(M/A))
    clrs = repmat(col, L, 1)
    
    # Create dict from Sobol effects for plotting 
    Sobol_raw = {'Main': tuple(Si),
                 'Total': tuple(STi),
                 'Interactions': tuple(STi - Si)}
    
    # Round all values in the dictionary
    num_digits = 2
    Sobol = {key: tuple(round(val, num_digits) for val in values)
                                          for key, values in Sobol_raw.items()}
    
    x = np.arange(len(X_Labels))  # the label locations
    width = 0.25                  # the width of the bars
    multiplier = 0
    
    # New axis when no axis exists (allows plot inside subplots)
    if ax is None: 
        fig, ax = plt.subplots(layout='constrained')
    
    
    for attribute, measurement in Sobol.items():    
        offset = width * multiplier
        rects = ax.bar(x + offset, measurement, width, label=attribute, 
                                                       color=clrs[multiplier])
        ax.bar_label(rects, padding=3)
        multiplier += 1
    
    # Adjust axes settings 
    ax.set_ylabel('effects')
    ax.legend(loc='upper left', ncols=1)
    ax.set_xticks(x + width, X_Labels)
    ax.set_ylim(-0.3, 1.25)
    plt.grid(axis='x')
    plt.axhline(y=0, color='black', linestyle=':')



def plot_VBSA(Si, STi,   # Sobols' indicees all samples
              Sic, STic, # Sobols' indicees across bootstrap resamples (1/3 N)
              # Mean & confidence intervals of indices across bootstrap resamples
              Si_m, Si_lb, Si_ub,       # Main effects
              STi_m, STi_lb, STi_ub,    # Total effects
              Inti_m, Inti_lb, Inti_ub, # Interactions
              NN, M, X_Labels,
              scalar_output, output_var_name):
    
    """
    Visualise the results of variance-based sensitivity analysis (VBSA).
    The function creates six subplots for VBSA analysis: Sobol indices, 
    convergence plots, means and confidence intervals of indices, and 
    interaction plots. It uses some of the SAFE toolbox plotting functions for 
    visualisation.

    Args:
        Si (array): Sobol indices for main effects of all samples.
        STi (array): Sobol indices for total effects of all samples.
        ------------- Indices across bootstrap resamples ----------------------
        Sic (array): Sobol indices for main effects.
        STic (array): Sobol indices for total effects. 

        Si_m (array): Mean values of main effects. 
        Si_lb (array): Lower bounds of confidence intervals for main effects.                                           
        Si_ub (array): Upper bounds of confidence intervals for main effects. 
                                                    
        STi_m (array): Mean values of total effects. 
        STi_lb (array): Lower bounds of confidence intervals for total effects.                                          
        STi_ub (array): Upper bounds of confidence intervals for total effects. 
                                                    
        Inti_m (array): Mean values of interaction effects 
        Inti_lb (array): Lower bounds of confidence intervals for interaction 
                                                                       effects.
        Inti_ub (array): Upper bounds of confidence intervals for interaction 
                                                                       effects.
        -----------------------------------------------------------------------                                                               
        NN (array): Array of model scores (for xtick of axes).
        M (int): Number of parameters being analysed.
        X_Labels (list): Labels for the input variables.
        scalar_output (str): Name of variable that is analysed.
        output_var_name (str): Name of variable set that is analyzed.

        
    Returns:
        None
        
    Note: The function generates, displays and saves the plot to a PDF file in 
          plots/VBSA_main_{scalar_output}_{output_var_name}.pdf
        
    Example: 
        >>>  plot_VBSA(Si, STi, Sic, STic, Si_m, Si_lb, Si_ub, 
                  STi_m, STi_lb, STi_ub, Inti_m, Inti_lb, Inti_ub,
                  NN, M, X_Labels, scalar_output, output_var_name)
    """

    import safepython.plot_functions as pf # module to visualize the results
    import matplotlib.pyplot as plt
        
    set_figure_properties()

    plt.figure(figsize = fig_size_large_rectangle)
    
    ax1 = plt.subplot(231)
    ax1.set_title('Sobol indices')
    plot_VBSA_indices (Si, STi, X_Labels, M, ax1)

     
    ax2 = plt.subplot(232)
    ax2.set_title('Convergence main effects')
    pf.plot_convergence(Sic, NN*(M+2), X_Label='No. of model evaluations',
                            Y_Label='Main effects', labelinput=X_Labels)
    new_xticks = NN*(M+2)
    ax2.set_xticks(new_xticks[1::2])

    
    ax3 = plt.subplot(233)
    ax3.set_title('Convergence total effects')
    pf.plot_convergence(STic, NN*(M+2), X_Label='No. of model evaluations',
                            Y_Label='Total effects', labelinput=X_Labels)
    new_xticks = NN*(M+2)
    ax3.set_xticks(new_xticks[1::2])
    ax3.get_legend().remove()
    
    # Set similar y-axis limits for upper plot row
    for ax in [ax1, ax2, ax3]:
        ax.set_ylim(-0.25, 1.25)
    
    header = '''
    --------------------------------------
    Mean and confidence intervals using bootstraping
    --------------------------------------
    '''
    # Format the text by removing newline characters and whitespace
    formatted_header = header.replace('\n', '').strip()
    
    ax4 = plt.subplot(234)
    pf.boxplot1(Si_m, S_lb=Si_lb, S_ub=Si_ub, X_Labels=X_Labels, 
                Y_Label='Main effects')
    
    ax5 = plt.subplot(235)
    ax5.set_title(formatted_header, pad=10)
    pf.boxplot1(STi_m, S_lb=STi_lb, S_ub=STi_ub, X_Labels=X_Labels, 
                Y_Label='Total effects')
    

    ax6 = plt.subplot(236)
    pf.boxplot1(Inti_m, S_lb=Inti_lb, S_ub=Inti_ub, X_Labels=X_Labels, 
                Y_Label='Interactions')
    
    # Set similar y-axis limits for lower plot row
    for ax in [ax4, ax5, ax6]:
        ax.set_ylim(-0.5, 1.2)
    
    set_latex_font()
    
    # Adjust global plot settings and export figure to PDF
    plt.subplots_adjust(hspace=0.45, wspace=0.35)
    filename = f"plots/VBSA_{scalar_output}_{output_var_name}.pdf"
    plt.savefig(filename, format="pdf", bbox_inches="tight")
    plt.show()  
    
    # Add additional plot with main effect and confidence interval for paper
    fig = plt.figure(figsize=fig_size_small_rectangle)
    ax = fig.add_subplot(111)
    pf.boxplot1(Si_m, S_lb=Si_lb, S_ub=Si_ub, X_Labels=X_Labels, Y_Label='Main effects')  
    ax.set_ylim(-0.3, 1.25)
    
    # Add bar plot values as number
    for j in range(M):
        ax.text(j + 1, Si_ub[j], f'{Si_m[j]:.2f}', ha='center', va='bottom')
    
    set_latex_font()
    
    filename = f"plots/VBSA_main_{scalar_output}_{output_var_name}.pdf"
    plt.savefig(filename, format="pdf", bbox_inches="tight")
    plt.show()  

#%% Plots for elementary effect (EE) analysis       
def plot_EE(mi, sigma, #                   # mean and standard deviation of EEs
            # Mean & confidence intervals of indices across bootstrap resamples
            mi_m, mi_lb, mi_ub,            # mean EEs
            sigma_m, sigma_lb, sigma_ub,   # standart deviation
            # Mean & confidence intervals for a decreasing number of samples
            mic, mic_m, mic_lb, mic_ub, rr,   
            M, X_Labels, scalar_output, output_var_name):
    """
    Visualise the results of elementary effects analysis (EET).
    This function plots various components of VBSA analysis, including Sobol 
    indices, convergence plots, means and confidence intervals of indices, and 
    interaction plots. It uses some of the SAFE toolbox plotting functions for 
    visualisation.

    Args:
        mi (array): Mean of EEs.
        sigma (array): Standard deviations of EEs.
        ------------- Indices across bootstrap resamples ----------------------
        mi_m (array): Mean of EEs.
        mi_lb (array): Lower bounds for means.
        mi_ub (array): Upper bounds for means.
        
        sigma_m (array): Standard deviations of EEs.
        sigma_lb (array): Lower bounds for standard deviations.
        sigma_ub (array): Upper bounds for standard deviations.
        -----------------------------------------------------------------------
        ---------- Indices for decreasing number of samples -------------------
        mic (array):  Mean of EEs (for convergence plot)
        mic_m (array): Mean of means of EEs.
        mic_lb (array): Lower bounds for means (convergence plot).
        mic_ub (array): Upper bounds for means (convergence plot).
        
        rr (float): Sample sizes to estimated reduced effects.
        -----------------------------------------------------------------------
        M (int): Number of iterations.
        X_Labels (list): List of labels for x-axis.
        scalar_output (str): Name of variable that is analysed.
        output_var_name (str): Name of variable set that is analyzed.

    Returns:
        None
        
    Note: The function generates, displays and saves the plot to a PDF file in 
          plots/EET_main_{scalar_output}_{output_var_name}.pdf
        
    Example: 
        >>>  plot_EE(mi, sigma, mi_m, mi_lb, mi_ub, sigma_m, sigma_lb, sigma_ub,   
                 mic, mic_m, mic_lb, mic_ub, rr, M, X_Labels)
    """
    
    import safepython.EET  as EET 
    import safepython.plot_functions as pf
    import matplotlib.pyplot as plt

    set_figure_properties()
    
    plt.figure(figsize=fig_size_medium_rectangle)
    
    # Plots without confidence interval ---------------------------------------
    # EET.EET_plot(mi, sigma, X_Labels) 
    # pf.plot_convergence(mic, rr*(M+1), X_Label='No. of model evaluations',
    #                     Y_Label='Mean of EEs', labelinput=X_Labels)

    ax3 = plt.subplot(121)
    EET.EET_plot(mi_m, sigma_m, X_Labels, mi_lb, mi_ub, sigma_lb, sigma_ub)
    ax3.set_ylabel('SD of EEs')
    ax3.get_legend().remove()

    plt.subplot(122)
    pf.plot_convergence(mic_m, rr*(M+1), mic_lb, mic_ub, 
                        X_Label='No. of model evaluations', 
                        Y_Label='Mean of EEs', labelinput=X_Labels)
    # ax4.get_legend().remove()
    
    
    set_latex_font()
    
    # Adjust global plot settings and export figure to PDF
    plt.subplots_adjust(hspace=0.25, wspace=0.3)
    filename = f"plots/EET_{scalar_output}_{output_var_name}.pdf"
    plt.savefig(filename, format="pdf", bbox_inches="tight")
    plt.show()  
    
    # Add additional plot with main effect and confidence interval for paper
    plt.figure(figsize=fig_size_small_rectangle)
    ax1 = plt.subplot(111)
    EET.EET_plot(mi_m, sigma_m, X_Labels, mi_lb, mi_ub, sigma_lb, sigma_ub)
    ax1.set_ylabel('SD of EEs')
    ax1.get_legend().remove()
    ax1.legend(X_Labels, loc='upper center', ncol=3, bbox_to_anchor=(0.5, 1.3), 
               columnspacing=0.6, handlelength=0.2) # frameon=False, fancybox=True
    
    # Add bar plot values as number
    # Calculate equally spaced y-coordinates
    # pos_y = plt.yticks()[0]
    y_coords = [min(sigma_m)*0.9, min(sigma_m)+(max(sigma_m)-min(sigma_m))/2, max(sigma_m)*1.1]
    mi_sort = sorted(mi_m)
    va = ['top','center','bottom']
    for i in range(M):
        ax1.text(mi_sort[i], y_coords[i], f'{mi_sort[i]:.1f}', ha='center', va=va[i])
        
    set_latex_font()
    
    filename = f"plots/EET_main_{scalar_output}_{output_var_name}.pdf"
    plt.savefig(filename, format="pdf", bbox_inches="tight")
    plt.show()  

#%% Plots for Fourier amplitude sensitivity testing (FAST)
def plot_FAST_indices(Si_fast, X_Labels, M, scalar_output, output_var_name, ax=None):
    """
    Generate a bar plot for the first order Sobol index estimated using Fourier 
    amplitude sensitivity testing (FAST).

    Args:
        Si (array): Estimated Sobol indices for the main effects.
        X_Labels (list): Labels for the input variables.
        M (int): Number of parameters being analysed.
        scalar_output (str): Name of variable that is analysed.
        output_var_name (str): Name of variable set that is analyzed.
        ax (matplotlib.axes.Axes, optional): Axes object upon which to plot the indices. 
            If not specified, a new figure and axes are created.

    Returns:
        None
        
    Note: The function generates, displays and saves the plot to a PDF file in 
          plots/FAST_{scalar_output}_{output_var_name}.pdf
        
    Example: 
        >>> plot_FAST_indices(Si_fast, X_Labels, M, ax1)
    """

    import matplotlib.pyplot as plt
    import numpy as np
    from numpy.matlib import repmat

    set_figure_properties()
    
    # 1x4 scatter plots (30, 4.5) cm 
    # fig_cm = (6/2.54, 4.5/2.54) # convert cm to inch
    fig = plt.figure(figsize=fig_size_small_rectangle)
    
    # Define plot colors (copied from SAFE toolbox plot functions)
    col = np.array([[228, 26, 28], [55, 126, 184], [77, 175, 74],
                   [152, 78, 163], [255, 127, 0]])/256
    
    A = len(col)
    L = int(np.ceil(M/A))
    clrs = repmat(col, L, 1)
    
    # Round all values in the dictionary
    num_digits = 2
    Si_fast_round = [round(val, num_digits) for val in Si_fast]      
    
    # New axis when no axis exists (allows plot inside subplots)
    if ax is None:
        ax = fig.add_subplot(111)
        
    rects = ax.bar(np.arange(M), Si_fast_round, color=clrs)
    ax.bar_label(rects)
    ax.set_ylabel('Main effects')
    ax.set_xticks(np.arange(M), X_Labels)
    ax.set_ylim(-0.3, 1.25)
    plt.grid(axis='x')
    plt.axhline(y=0, color='black', linestyle=':')
    
    set_latex_font()
    
    # Adjust global plot settings and export figure to PDF
    filename = f"plots/FAST_{scalar_output}_{output_var_name}.pdf"
    plt.savefig(filename, format="pdf", bbox_inches="tight")
    plt.show()
    

#%% Plots for elementary effect (EE) analysis       
def plot_PAWN (KS_median_m, KS_median_lb, KS_median_ub,
               KS_mean_m, KS_mean_lb, KS_mean_ub,
               KS_max_m, KS_max_lb, KS_max_ub,
               KS_median_c_m, KS_median_c_lb, KS_median_c_ub,
               KS_mean_c_m, KS_mean_c_lb, KS_mean_c_ub,
               KS_max_c_m, KS_max_c_lb, KS_max_c_ub,
               NN, X_Labels, scalar_output, output_var_name):
    # KS_median, KS_mean, KS_max,
    # KS_median_boot, KS_mean_boot, KS_max_boot,
    # KS_median_c, KS_mean_c, KS_max_c,
    # KS_median_c_boot, KS_mean_c_boot, KS_max_c_boot,
    """
    Visualise the results of PAWN.
    This function plots various components of VBSA analysis, including Sobol 
    indices, convergence plots, means and confidence intervals of indices, and 
    interaction plots. It uses some of the SAFE toolbox plotting functions for 
    visualisation.

    Args: KS_median_m, KS_median_lb, KS_median_ub,
          KS_mean_m, KS_mean_lb, KS_mean_ub,
          KS_max_m, KS_max_lb, KS_max_ub,
          KS_median_c_m, KS_median_c_lb, KS_median_c_ub,
          KS_mean_c_m, KS_mean_c_lb, KS_mean_c_ub,
          KS_max_c_m, KS_max_c_lb, KS_max_c_ub,
          NN, X_Labels, scalar_output, output_var_name 

    Returns:
        None
        
    Note: The function generates, displays and saves the plot to a PDF file in 
          plots/PAWN_main_{scalar_output}_{output_var_name}.pdf
        
    Example: 
        >>>  plot_PAWN (KS_median_m, KS_median_lb, KS_median_ub,
                       KS_mean_m, KS_mean_lb, KS_mean_ub,
                       KS_max_m, KS_max_lb, KS_max_ub,
                       KS_median_c_m, KS_median_c_lb, KS_median_c_ub,
                       KS_mean_c_m, KS_mean_c_lb, KS_mean_c_ub,
                       KS_max_c_m, KS_max_c_lb, KS_max_c_ub,
                       NN, X_Labels, scalar_output)
    """

    import safepython.plot_functions as pf
    import matplotlib.pyplot as plt


    set_figure_properties()
    
    plt.figure(figsize=fig_size_large_rectangle)

    # Plots without confidence interval ---------------------------------------
    # pf.boxplot1(KS_median, X_Labels=X_Labels, Y_Label='KS (median)')
    # pf.boxplot1(KS_mean, X_Labels=X_Labels, Y_Label='KS (mean)')
    # pf.boxplot1(KS_max, X_Labels=X_Labels, Y_Label='Ks (max)')
    
    # CFD mean without confidence interval ------------------------------------
    # pf.plot_convergence(KS_median_c, NN, X_Label='no of model evaluations',
    #                     Y_Label='KS (median)', labelinput=X_Labels)
    # pf.plot_convergence(KS_median_c, NN, X_Label='no of model evaluations',
    #                     Y_Label='KS (mean)', labelinput=X_Labels)
    # pf.plot_convergence(KS_max_c, NN, X_Label='no of model evaluations',
    #                     Y_Label='KS (max)', labelinput=X_Labels)
    
    
    # PAWN sensitivity indices mean and confidence intervals using bootstraping
    plt.subplot(231) 
    pf.boxplot1(KS_median_m, S_lb=KS_median_lb, S_ub=KS_median_ub, 
                X_Labels=X_Labels, Y_Label='KS (median)')
    
    plt.subplot(232)
    pf.boxplot1(KS_mean_m, S_lb=KS_mean_lb, S_ub=KS_mean_ub,
                X_Labels=X_Labels, Y_Label='KS (mean)')
    
    plt.subplot(233)
    pf.boxplot1(KS_max_m, S_lb=KS_max_lb, S_ub=KS_max_ub,
                X_Labels=X_Labels, Y_Label='Ks (max)')
    
    # CFD Mean and confidence intervals using bootstraping
    plt.subplot(234)
    pf.plot_convergence(KS_median_c_m, NN, KS_median_c_lb, KS_median_c_ub,
                        X_Label='no of model evaluations',
                        Y_Label='KS (median)', labelinput=X_Labels)
    # legend_ax4.set_bbox_to_anchor((0.3, 1))  # New legend location

    ax5 = plt.subplot(235)
    pf.plot_convergence(KS_mean_c_m, NN, KS_mean_c_lb, KS_mean_c_ub,
                        X_Label='no of model evaluations',
                        Y_Label='KS (mean)', labelinput=X_Labels)
    ax5.get_legend().remove()
    
    
    ax6 = plt.subplot(236)
    pf.plot_convergence(KS_max_c_m, NN, KS_max_c_lb, KS_max_c_ub,
                        X_Label='no of model evaluations',
                        Y_Label='KS (max)', labelinput=X_Labels)
    ax6.get_legend().remove()
    
    set_latex_font()
    
    # Adjust global plot settings and export figure to PDF
    plt.subplots_adjust(hspace=0.3, wspace=0.4)
    filename = f"plots/PAWN_{scalar_output}_{output_var_name}.pdf"
    plt.savefig(filename, format="pdf", bbox_inches="tight")
    plt.show()  
    
    # Add additional plot with main effect and confidence interval for paper
    fig = plt.figure(figsize=fig_size_small_rectangle)
    ax = fig.add_subplot(111)
    pf.boxplot1(KS_mean_m, S_lb=KS_mean_lb, S_ub=KS_mean_ub,
                X_Labels=X_Labels, Y_Label='KS (mean)')
    ax.set_ylim(-0.3, 1.25)
    
    # Add bar plot values as number
    for j in range(len(X_Labels)):
        ax.text(j + 1, KS_mean_ub[j], f'{KS_mean_m[j]:.2f}', ha='center', va='bottom')
    
    set_latex_font()
    
    filename = f"plots/PAWN_main_{scalar_output}_{output_var_name}.pdf"
    plt.savefig(filename, format="pdf", bbox_inches="tight")
    plt.show()  
    
#%% Paralell Coordinates Plot - still in developement!!
def plot_parallel_coordinates(Y_all_eval, X_all, scalar_output, X_Labels):
    """
    Generate a parallel coordinates plot.

    Parameters:
    - Y_all_eval (array-like): Array of evaluation values.
    - X_all (array-like): Array of input values.
    - scalar_output (str): Label for the scalar output.
    - X_Labels (list): List of labels for X_all dimensions.

    Returns:
        None
    """
    import pandas as pd
    import matplotlib.pyplot as plt
    from pandas.plotting import parallel_coordinates
    
    df = pd.DataFrame({
        X_Labels[0]: X_all[:, 0],
        X_Labels[1]: X_all[:, 1],
        X_Labels[2]: X_all[:, 2],
        scalar_output: Y_all_eval
    })

    change = round((max(df[scalar_output]) - min(df[scalar_output])) / 8, 2)

    def truncate(value, step):
        return round(round(value / step) * step, 2)

    df[scalar_output] = truncate(df[scalar_output], step=change)

    fig, ax = plt.subplots(figsize=(10, 6))
    parallel_coordinates(df[::5][df[scalar_output].notna()], scalar_output, sort_labels=True, colormap='jet')

    plt.show()

    

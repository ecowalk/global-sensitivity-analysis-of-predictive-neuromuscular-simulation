# -*- coding: utf-8 -*-
"""
Sensitivity analysis (SA) for a predictive neuromuscular gait model [5]. 

Software requirements: 
    - SCONE[1] with Hyfydy[2] (licence required!) 
    - SAFE Toolbox [3,4]
    - Python version 3.9
    - Packages: numpy [6], scipy [7], matplotlib [8], 
                numba [9], pandas [10], os [11]


Workflow:
    Step 1: Define parameters (experimental set-up), select SA method, sampling
    ----------------- ** see scone_model_execution.py ** ----------------------
    Step 2: Run the model and compute the desired model outputs
    ---------------------------------------------------------------------------
    Step 3: Evaluate model outputs with SAFE toolbox and individual plots

    
Supported SA methods (see also documentation of the SFAE Toolbox in [3,4])
    - eet (elementary effects test or Morris method)
    - fast (Fourier amplitude sensitivity test)
    - pawn (uses cumulative distribution functions (CDFs))
    - vbsa (variance-based sensitivity analysis, or the Sobol' method)

    
###############################################################################
References:

[1] Geijtenbeek, Thomas (2019): SCONE. Open Source Software for Predictive 
    Simulation of Biological Motion. In: JOSS 4 (38), S. 1421. DOI: 10.21105/joss.01421.
[2] https://hyfydy.com/ and https://hyfydy.com/documentation
[3] Pianosi, Francesca; Sarrazin, Fanny; Wagener, Thorsten (2015): A Matlab 
    toolbox for Global Sensitivity Analysis. In: Environmental Modelling & 
    Software 70, S. 80–85. DOI: 10.1016/j.envsoft.2015.04.009.
[4] https://github.com/SAFEtoolbox/SAFE-python
[5] Geyer, Hartmut; Herr, Hugh (2010): A muscle-reflex model that encodes 
    principles of legged mechanics produces human walking dynamics and muscle 
    activities. In: IEEE Transactions on Neural Systems and Rehabilitation 
    Engineering : a Publication of the IEEE Engineering in Medicine and Biology 
    Society 18 (3), S. 263–273. DOI: 10.1109/TNSRE.2010.2047592.
[6] https://numpy.org/
[7] https://scipy.org/
[8] https://matplotlib.org/
[9] https://numba.pydata.org/
[10] https://pandas.pydata.org/
[11] https://docs.python.org/3/library/os.html
    
###############################################################################

 ------------------------------------------------------------------------------
 
 © Alexandra Buchmann, Chair of Applied Mechanics, alexandra.buchmann@tum.de
 
 Disclaimer: The author is not responsible for the content of external links or 
 publications. We have tested this code to the best of our knowledge. However, 
 we cannot take any legal responsibility for errors in coding or malfunctions. 
 You should always check the results for accuracy and report any problems to 
 the author.
 
 Parts of this code were created using ChatGPT and Stack Overflow.
 
 Feedback and comments are welcome at alexandra.buchmann@tum.de. 
 
 ------------------------------------------------------------------------------
 Initiaized:       Mon Apr 24 12:55:07 2023, Munich
 Last modified:    04-01-2024, Munich 
 ------------------------------------------------------------------------------
"""

#%% Step 1: Parameter definition (experimental set up) and method selection
import numpy as np
import scipy.stats as st
from safe_sampling import sample_inputs

'''
Parameters for GAS, SOL, HFL (active during terminal stance and preswing):
    - muscle stiffness (ratios): 0-500%, 0.01-5
        r'gastroc_.\s*{\s*stiffness_multiplier.factor',
        r'soleus_.\s*{\s*stiffness_multiplier.factor',
        r'iliopsoas_.\s*{\s*stiffness_multiplier.factor'
        
    - muscle slack lengths: +-10%
        r'gastroc_.\s*{\s*tendon_slack_length.factor',
        r'soleus_.\s*{\s*tendon_slack_length.factor',
        r'iliopsoas_.\s*{\s*tendon_slack_length.factor'
        
        Info on nominal values:
        # GAS tendon_slack_length = 0.39  - (ce_len) bounds=[0.006, 0.12]
        # SOL tendon_slack_length = 0.25  - (ce_len) bounds=[0.005, 0.1]
        # IlO tendon_slack_length = 0.163 - (ce_len) bounds=[0.01, 0.2] 
        
    - actuation points (CAM radii) around the knee and ankle joint:
        [r'\$calcn_x_pos_GAS',
         r'\$calcn_x_pos_SOL',
         r'\$femur_x_pos_GAS'] - range for min/max: [0.1, 0.1, 0.02]*0.9 or 1.1
'''

X_Labels = ['GAS', 'SOL', 'HFL'] 

Model_Labels = [] 

SCONE_Labels = [
    r'gastroc_.\s*{\s*tendon_slack_length.factor',
    r'soleus_.\s*{\s*tendon_slack_length.factor',
    r'iliopsoas_.\s*{\s*tendon_slack_length.factor'
] 

output_var_name = 'l_slack' # name for output files of plotting

xmin = np.array([1, 1, 1]) * 0.9 # Parameter ranges
xmin = xmin.astype(float)
xmax = np.array([1, 1, 1]) * 1.1
xmax = xmax.astype(float)


dimension_check = len(X_Labels) == (len(SCONE_Labels) + len(Model_Labels)) or \
                  len(X_Labels) == xmin.size or \
                  len(X_Labels) == xmax.size or \
                  xmin.size == xmax.size
      
if not dimension_check: 
    raise ValueError('Inconsistent dimensions for labels and/or parameter boundaries.') 
    
    
print("\n" + "*" * 60)
print("\n" + "#" * 20 + " SA Settings " + "#" * 27) # -------------------------
   
samp_strat = 'lhs' # Latin Hypercube (Saltelli, 2008; details, see VBSA.vbsa_resampling or VBSA.vbsa_indices)
GSA_met = 'PAWN' # GSA method: EET, FAST, PAWN, VBSA 
                             
N = 2000 # Number of samples; must be odd for FAST
Nboot = int(np.round(N/3,0)) # Number of bootstrap samples for convergence analysis

M = len(X_Labels) # Number of uncertain parameters subject to SA

r = 200 # For EET method only: number of Elementary Effects 
design_type = 'radial' # Second option: 'trajectory'


print(f'''-Sample size: {N} \n-Number of EEs: {r} \n-Number of Parameters: {M} 
      \n-Method: {GSA_met} \n-Sampling: {samp_strat} 
      \n-Parameter: {output_var_name} \n-Min. Value: {min(xmin)} \n-Max. Value: {max(xmax)}''') 
       
distr_fun = st.uniform  
distr_par = [np.nan] * M  
for i in range(M):
    distr_par[i] = [xmin[i], xmax[i] - xmin[i]] # Shape parameters for uniform dist

X_all = sample_inputs(M, N, samp_strat, distr_fun, distr_par, GSA_met, r, design_type)

#%% Step 2: Create optimization scenarios (.scone files) and optimize 
from scone_model_execution import model_execution_SCONE
from scone_batch_workflow import delete_files, clear_folder
import os
import pandas as pd

print("\n" + "-" * 20 + " Model Execution Start" + "-" * 18) # ----------------

model_execution_config = {
    'flag_genfiles':    True,  # Generate new SCONE files from X_all samples
    'flag_optim':       True,  # Start optimization for all new files in optim_dir
    'flag_eval':        True,  # Evaluate results for all files in results_dir with SCONE
    'scenario' : "resources/data/SA_base.scone", # Reference scenario file 
    'model'    : "resources/data/H0914.hfd",     # Reference model file 
    'results_dir': '../results' , # Directory in which SCONE stores optimization results (see Tools-Preferences-General)
    'optim_dir': "resources/scenarios",  # Directory to store generated .scone scenario files
    'scenario_name': "SA.scone", # Name for generated .scone files 
    'model_dir': "resources/models",     # Directory to store generated .hfd modles 
    'model_name': "H0914.hfd"
}


print("\n" + "#" * 20 + " Clear Files (New Optim) " + "#" * 15) # -------------

if model_execution_config['flag_genfiles'] & model_execution_config['flag_optim']:
    delete_files('resources/models', '.hfd')
    delete_files('resources/scenarios', '.scone')
    delete_files('resources/scenarios', '.bat')
    clear_folder('../results')

Y_all, out_df_sto, out_df_res = model_execution_SCONE(
    X_all, SCONE_Labels,Model_Labels, model_execution_config
)

print("\n" + "-" * 20 + " Model Execution End " + "-" * 19) # -----------------

# Save output data to .csv data for later analysis or documentation 
flag_save_data = True
if flag_save_data:
    print("\n" + "#" * 20 + " Saving Output Files " + "#" * 19) # -------------
    X_all_df = pd.DataFrame(X_all, columns=X_Labels)
    X_all_df.to_csv(f'output_files/X_all_{GSA_met}_{output_var_name}.csv', index=False)
    Y_all.to_csv(f'output_files/Y_all_{GSA_met}_{output_var_name}.csv', index=False)
    out_df_res.to_csv(f'output_files/out_df_res_{GSA_met}_{output_var_name}.csv', index=False)
    
    df_sto_folder_name = f'output_files/out_df_sto_{GSA_met}_{output_var_name}'
    os.makedirs(df_sto_folder_name, exist_ok=True)
    
    for i, df in enumerate(out_df_sto):
        df.to_csv(f'{df_sto_folder_name}/{i}.csv', index=False)
    

#%% Step 3: Evaluate model outputs 
import safepython.VBSA as VB 
import safepython.EET  as EET 
import safepython.FAST as FAST 
import safepython.PAWN as PAWN 
from safepython.util import aggregate_boot 

# import matplotlib.pyplot as plt
import warnings
import pandas as pd
import os

from safe_plotting import ( 
    plot_VBSA, plot_EE, plot_FAST_indices, plot_PAWN,
    scatter3d_with_projections
    ) #plot_parallel_coordinates, 

# Load data in case you do not want to simulation again
flag_load = False
if flag_load:
    df_sto_folder_name = f'output_files/out_df_sto_{GSA_met}_{output_var_name}'
    X_all_df = pd.read_csv(f'output_files/X_all_{GSA_met}_{output_var_name}.csv')
    X_all = X_all_df.to_numpy()
    Y_all = pd.read_csv(f'output_files/Y_all_{GSA_met}_{output_var_name}.csv')
    out_df_res = pd.read_csv(f'output_files/out_df_res_{GSA_met}_{output_var_name}.csv')
    
    # Load out_sto files from folder 
    csv_files = sorted([file for file in os.listdir(df_sto_folder_name) if file.endswith('.csv')],
                       key=lambda x: int(x.split('.')[0])) # sort according to file numer
    
    # Init
    out_df_sto = [] 
    for csv_file in csv_files:
        file_path = os.path.join(df_sto_folder_name, csv_file)
        df = pd.read_csv(file_path)
        out_df_sto.append(df)
        
    
# Loop through all selected output criteria
for item in ['APO_AMP', 'APO_max', 'Step_Vel', 'E_CoT', 'R_RoM_Ank']:
    scalar_output = item # other options see Y_all headings
    Y_all_eval = np.array(Y_all[scalar_output])
    
    # Select quantity of interest for further analysis
    print("\n" + "#" * 20 + " Visualization " + "#" * 25) # -------------------
    print('Scatter plot') # ---------------------------------------------------
    scatter3d_with_projections (Y_all_eval, X_all, X_Labels, scalar_output, output_var_name, GSA_met)

    # print('Parallel coordinates plot') # ------------------------------------
    # plot_parallel_coordinates(Y_all_eval, X_all, scalar_output, X_Labels)

    print('SAFE Toolbox Plots') # ---------------------------------------------
    if GSA_met =='VBSA': 
    
        YA, YB, YC = np.split(Y_all_eval, [N, N+N], axis=0) 

        Si, STi = VB.vbsa_indices(YA, YB, YC, M)  # First-order and total effects
        
        NN = np.linspace(N/10, N, 10).astype(int)
        Sic, STic = VB.vbsa_convergence(YA, YB, YC, M, NN) # Convergence analysis
        
        # Mean and confidence intervals for SA indices using bootstraping 
        Si_boot, STi_boot = VB.vbsa_indices(YA, YB, YC, M, Nboot=Nboot)
        Si_m, Si_lb, Si_ub = aggregate_boot(Si_boot)                # shape (M,)
        STi_m, STi_lb, STi_ub = aggregate_boot(STi_boot)            # shape (M,)
        Inti_m, Inti_lb, Inti_ub = aggregate_boot(STi_boot-Si_boot) # shape (M,)
    
        # Plot VBSA results 
        plot_VBSA(Si, STi, Sic, STic, Si_m, Si_lb, Si_ub, 
                  STi_m, STi_lb, STi_ub, Inti_m, Inti_lb, Inti_ub,
                  NN, M, X_Labels, scalar_output, output_var_name)
        
        
    elif GSA_met == 'EET': 
        # mi: mean of the Elementary Effects (EEs) (input influence)
        # sigma: standard deviation of EEs (level of interactions with other inputs)
        mi, sigma, _ = EET.EET_indices(r, list(xmin), list(xmax), X_all, Y_all_eval, design_type)
        
        # Sensitivity indices, their mean and confidence intervals with bootstraping 
        mi_boot, sigma_boot, EE_boot = EET.EET_indices(r, list(xmin), list(xmax), X_all, 
                                                       Y_all_eval, design_type, 
                                                       Nboot=Nboot)
        mi_m, mi_lb, mi_ub = aggregate_boot(mi_boot)             # shape (M,)
        sigma_m, sigma_lb, sigma_ub = aggregate_boot(sigma_boot) # shape (M,)
    
        
        # Repeat computations with decreasing number of samples so as to assess
        # if convergence was reached within the available dataset:
        rr = np.linspace(r/5, r, 5).astype(int) # Sample sizes for index estimation
        # mic, sigmac: i-th element corresponds to indices at the i-th sample size
        mic, sigmac = EET.EET_convergence(EE_boot, rr)               
        
        # Mean and confidence intervals for SA indices using bootstraping
        mic_boot, sigmac_boot = EET.EET_convergence(EE_boot, rr, Nboot) 
        mic_m, mic_lb, mic_ub = aggregate_boot(mic_boot)             # shape (R,M)
        sigmac_m, sigmac_lb, sigmac_ub = aggregate_boot(sigmac_boot) # shape (R,M)
        
        # Plot EE results 
        plot_EE(mi, sigma, mi_m, mi_lb, mi_ub, sigma_m, sigma_lb, sigma_ub,   
                 mic, mic_m, mic_lb, mic_ub, rr, M, X_Labels, scalar_output, 
                 output_var_name)
        
        
    elif GSA_met == 'FAST':     
        # First-order effect estimate
        Si_fast, V, A, B, Vi = FAST.FAST_indices(Y_all_eval, M) 
        plot_FAST_indices(Si_fast, X_Labels, M, scalar_output, output_var_name)
        
       
        # Convergence analysis skipped as it requires additional model executions
        # N_fast = len(Y_all_eval)
        # NNfast = np.linspace(N_fast, N_fast+5000, 6).astype(int)
        # Si_fast_conv = np.nan * np.zeros((len(NNfast), M))
        # Si_fast_conv[0, :] = Si_fast
        
        # for n in range(1, len(NNfast)):
        #     Xn, sn = FAST.FAST_sampling(distr_fun, distr_par, M, NNfast[n])
        #     Yn = model_execution(fun_test, Xn, rain, evap, flow, warmup)
        #     Si_fast_conv[n, :], _, _, _, _ = FAST.FAST_indices(Yn, M)
        
        # # Plot results:
        # plt.figure()
        # pf.plot_convergence(Si_fast_conv, NNfast, X_Label='no of model evaluations',
        #                     Y_Label='1st-order sensitivity', labelinput=X_Labels)
        # plt.show()
        
    
    elif GSA_met == 'PAWN':
        n = 10 # number of conditioning intervals
        
        # Mean and confidence intervals for SA indices using bootstraping
        KS_median_boot, KS_mean_boot, KS_max_boot = PAWN.pawn_indices(X_all, Y_all_eval, n, Nboot=Nboot)
        KS_median_m, KS_median_lb, KS_median_ub = aggregate_boot(KS_median_boot) # shape (M,)
        KS_mean_m, KS_mean_lb, KS_mean_ub = aggregate_boot(KS_mean_boot) # shape (M,)
        KS_max_m, KS_max_lb, KS_max_ub = aggregate_boot(KS_max_boot) # shape (M,)
        
        # Analyze convergence of sensitivity indices:
        NN = np.linspace(N/5, N, 5).astype(int)
        
        # Mean and confidence intervals for SA indices over no. evaluations
        KS_median_c_boot, KS_mean_c_boot, KS_max_c_boot = PAWN.pawn_convergence(X_all, Y_all_eval, n, NN, Nboot)
        KS_median_c_m, KS_median_c_lb, KS_median_c_ub = aggregate_boot(KS_median_c_boot) # shape (R,M)
        KS_mean_c_m, KS_mean_c_lb, KS_mean_c_ub = aggregate_boot(KS_mean_c_boot)         # shape (R,M)
        KS_max_c_m, KS_max_c_lb, KS_max_c_ub = aggregate_boot(KS_max_c_boot)             # shape (R,M)
        
        
        plot_PAWN (KS_median_m, KS_median_lb, KS_median_ub,
                   KS_mean_m, KS_mean_lb, KS_mean_ub,
                   KS_max_m, KS_max_lb, KS_max_ub,
                   KS_median_c_m, KS_median_c_lb, KS_median_c_ub,
                   KS_mean_c_m, KS_mean_c_lb, KS_mean_c_ub,
                   KS_max_c_m, KS_max_c_lb, KS_max_c_ub,
                   NN, X_Labels, scalar_output, output_var_name)
        
        
        # Compute PAWN sensitivity indices:
        # KS_median, KS_mean, KS_max = PAWN.pawn_indices(X_all, Y_all_eval, n)
        # KS_median_c, KS_mean_c, KS_max_c = PAWN.pawn_convergence(X_all, Y_all_eval, n, NN) # shape (R,M)
        
        # Compute and plot conditional and unconditional CDFs:
        # YF, FU, FC, xc = PAWN.pawn_plot_cdf(X_all, Y_all_eval, n, cbar=True, n_col=3, 
        #                                    labelinput=X_Labels)
        # plt.subplots_adjust(left=None, bottom=None, right=None, top=None,
        #                    hspace=0.5, wspace=0.4)
        # plt.show()
    
    
        # KS = PAWN.pawn_plot_ks(YF, FU, FC, xc, n_col=3, X_Labels=X_Labels)
        # plt.subplots_adjust(left=None, bottom=None, right=None, top=None,
        #                    hspace=0.5, wspace=1)
        # plt.show()
        
        
    else: 
        warnings.warn(f'No method called {GSA_met}', UserWarning)
    
    print("\n" + "*" * 60)


# -*- coding: utf-8 -*-
"""
******************** Module scone_data_postprocess.py ********************

scone_data_postprocess.py provides a set of functions to read and postprocess 
output files from SCONE.

The module includes the following functions: 
    - read_sto_files_parallel(directory:str)
    - read_results_data(file_path:str, no_sim:int)
    
    - detect_step(df:pd.DataFrame, threshold:float)
    - get_ankle_details(df:pd.DataFrame)
    - get_cross_corr(df:pd.DataFrame)

NOTE: The functions are tailored to .par[1] (optimisation results) and .sto[2,3]
      (simulation results, time series) file formats.
      
      .par-structure: (:,4) - [parameter name,  best value, mean value, std]
      .sto-structure: file name of corresponding .par file
                      version=1
                      nRows=xxxx
                      nColumns=xxxx
                      inDegrees=no
                      endheader   
                      (nRows, nColumns) - [time var1 var2 var3 ...]


###############################################################################
References:

[1] https://scone.software/doku.php?id=doc:par
[2] https://scone.software/doku.php?id=doc:sto
[3] https://simtk-confluence.stanford.edu:8443/display/OpenSim/Storage+(.sto)+Files
    
###############################################################################

 ------------------------------------------------------------------------------
 
 © Alexandra Buchmann, Chair of Applied Mechanics, alexandra.buchmann@tum.de
 
 ------------------------------------------------------------------------------
 Initiaized:       Mon Apr 24 2023 13:31:56, Munich
 Last modified:    04-01-2024, Munich 
 ------------------------------------------------------------------------------
"""

import pandas as pd 
import numpy as np
import warnings
import os
import re

#%% Read raw data and results to DataFrame
def process_file(file_path):
    import pandas as pd
    df = pd.read_csv(file_path, sep='\t', skiprows=6)
    df = df.loc[:, (df != 0).any(axis=0)] # Drop columns with only zeros
    return df

def read_sto_files_parallel(directory: str):
    """
    Read ".par.sto" files from all subfolders in the given directory into list 
    of DataFrames using ThreadPool execution.
        
    Args: 
        directory (str): Directory where SCONE optimisation results are stored. 

    Returns:
        None
            
    Example: 
        >>> results_dir = "../results"
        >>> out_df_sto = read_par_sto_files(results_dir) 
    """
    from concurrent.futures import ThreadPoolExecutor
    
    dataframes = []

    with ThreadPoolExecutor() as executor: 
        file_paths = [
            os.path.join(root, name) 
            for root, dirs, files in os.walk(directory) # Scan directories top-down
            for name in sorted(files) 
            if name.endswith(".par.sto")
        ]
        
        dataframes = list(executor.map(process_file, file_paths))

    return dataframes


def read_results_data(folder_path:str, no_sim:int):
    """
    Read data from .txt files created by sconecmd-batch evaluation. Each .txt 
    file contains evaluation results that are displayed in 'messages' in SCONE.
    
    Args:
        folder_path (str): Path to results .txt files.
        no_sim (int): Expected number of records (to check if the .txt file was 
                                                  created successfully)

    Returns:
        pd.DataFrame: DataFrame of size (no_sim, 7)

    Raises:
        ValueError: If the actual number of data sets in the text file does not 
        match `no_sim`.
        
    Example: 
        >>> results_dir = "../results"
        >>> file_name = "results_all.txt"
        >>> out_df_res = read_results_data(os.path.join(results_dir, file_name), X.shape[0])  
    """
    
 
    data = {
        'result': [],
        'step_velocity': [],
        'step_count': [],
        'effort': [],
        'distance': [],
        'DofLimits': [],
        'GRF': [],
        'time': [] # simulation time
    } 
    
    files = [
        filename 
        for filename in os.listdir(folder_path) 
        if filename.startswith("results_") and filename.endswith(".txt")
    ]
    
    files.sort(key=lambda x: int(re.search(r'\d+', x).group())) # alphanumeric sorting
    
    
    for filename in files:
        file_path = os.path.join(folder_path, filename)
            
        with open(file_path, 'r') as file:
            for line in file:
                # Regular expression search: 'var_name = X.xx' with any spaces
                match = re.search(r'(\w+)\s+=\s+([\d.]+)', line) 
                if match:
                    key, value = match.group(1), float(match.group(2))
                    if key in data:
                        data[key].append(value)

    df = pd.DataFrame(data)
    
    if df.shape[0] != no_sim: 
        raise ValueError(
            f'''extract_results_data: The number of data sets in the results 
            file ({df.shape[0]}) does not match the expected number ({no_sim}). 
            Check evaluation procedure!'''
        )
    
    return df


#%% Post-process output data: step detection, ankle power, cross correlation
def detect_step(df:pd.DataFrame, threshold:float):
    """
    Step detection based on the vertical GRFs of the left leg. The start of a 
    step is defined when the GRFs rise above the given threshold.

    Args:
        df (pandas.DataFrame): DataFrame from imported .sto file.
        threshold (float):     GRF threshold value for step detection.

    Returns:
        int:  Number of steps.
        list: Corresponding indices of the steps in the DataFrame.
    
    Example:
        >>> df_sto = read_sto_files('../results')
        >>> steps, step_index = detect_step(df_sto,"leg0_l.grf_y",5)
    """

    steps = 0
    indices = []
    prev_value = df.iloc[0]["leg0_l.grf_y"]

    for index, row in df.iterrows():
        value = row["leg0_l.grf_y"]
        if value > threshold and prev_value <= threshold:
            steps += 1
            indices.append(index)
        prev_value = value

    indices = [x - 1 for x in indices] # Shift by one to get beginning of step
    
    return steps, indices


def get_ankle_details(df:pd.DataFrame):
    """
    Returns maximum ankle power and power amplififcation (max. / min.) during 
    the last full step of a simulation.

    Args:
        df (pandas.DataFrame): DataFrame with the simulation data (.sto).

    Returns:
        Tuple[float, float]: Maximum ankle power and power amplification during
        last full step of the simulation.
        Returns (0, 0) if any of the values is zero.

    Raises:
        TypeError: If df is not a pandas DataFrame.
        ValueError: If the DataFrame does not contain 'ankle_angle_l.power'.

    Example:
        >>> df_sto = read_sto_files('../results')
        >>> APO_max, P_amp = get_ankle_details(df_sto)
    """
    
    # Initialize 
    max_ankle_power = 0 # np.nan
    ankle_amp_ratio = 0 # np.nan
    
    steps, step_index = detect_step(df, 5)
    
    var_name = 'ankle_angle_l.power'
    column = df.columns.get_loc(var_name) if var_name in df.columns else None
    
    try:
        max_ankle_power = max(df.iloc[step_index[-2]:step_index[-1],column])
        
        power_column = df["ankle_angle_l.power"][step_index[-2]:step_index[-1]]
        
        negatives = power_column[power_column < 0]
        positives = power_column[power_column > 0]
        
        min_negative = negatives.min()
        max_positive = positives.max()
        
        ankle_amp_ratio = max_positive / abs(min_negative) 
        
    except:
        warnings.warn('get_ankle_details: Unable to calculate max ankle power.', UserWarning)

    return max_ankle_power, ankle_amp_ratio


def get_cross_corr(df:pd.DataFrame):
    """
    Calculates normalized cross correlation of ankle, knee, and hip kinematics
    to human data for the last full step of the left leg.

    Args:
        df (pandas.DataFrame): DataFrame with .sto output from SCONE.

    Returns:
        Tuple[float, float, float]: Tuple of R-value for ankle, knee, and hip. 
        Returns (0, 0, 0) if any of the values is zero.

    Raises:
        UserWarning: If calculation fails.

    Example:
        >>> get_cross_corr(df)
    """
    
    # Initialize 
    R_ank_norm = 0 #np.nan
    R_kne_norm = 0 #np.nan
    R_hip_norm = 0 #np.nan
    
    steps, step_index = detect_step(df, 5)

    # Human reference data
    norm_mean_hip = np.array([ 33.0, 33.0, 32.9, 32.7, 32.5, 32.2, 32.0, 31.6, 31.2, 30.6, 29.9, 29.0, 28.1, 27.1, 26.1, 25.1, 24.0, 23.0, 21.9, 20.9, 19.7, 18.6, 17.4, 16.2, 14.9, 13.7, 12.4, 11.2,  9.9,  8.7,  7.5,  6.3, 5.2,   4.1,  3.0,  1.9,  0.9, -0.1, -1.1, -2.0,  -2.9,   -3.7,  -4.5,  -5.2,  -5.8, -6.4, -6.8, -7.3, -7.6, -7.9, -8.2, -8.3, -8.3, -8.1, -7.8, -7.3, -6.7, -5.9, -4.9, -3.7, -2.3, -0.7, 1.1, 3.0, 5.0, 7.0, 9.0, 10.9, 12.9, 14.9, 16.7, 18.5, 20.2, 21.9, 23.4, 24.9, 26.3, 27.7, 28.9, 30.0, 30.9, 31.8, 32.5, 33.0, 33.4, 33.7, 33.7, 33.7, 33.5, 33.1, 32.8, 32.4, 32.0, 31.7, 31.5, 31.4, 31.4, 31.5, 31.7, 31.9, 32.0 ])
    norm_mean_kne =np.array([ 7.9, 9.3, 10.3, 11.0, 11.9, 13.1, 14.7, 16.2, 17.4, 18.3, 19.0, 19.3, 19.4, 19.3, 19.0, 18.5, 18.1, 17.6, 17.2, 16.7, 16.2, 15.7, 15.1, 14.6, 14.0, 13.4, 12.8, 12.1, 11.5, 10.9, 10.2, 9.6, 9.1, 8.6, 8.1, 7.8, 7.5, 7.3, 7.2, 7.2, 7.3, 7.4, 7.7, 8.1, 8.6, 9.3, 10.0, 10.9, 11.9, 12.9, 14.1, 15.5, 17.0, 18.7, 20.6, 22.8, 25.2, 27.9, 30.8, 33.9, 37.2, 40.5, 44.0, 47.3, 50.4, 53.2, 55.7, 57.9, 59.8, 61.3, 62.4, 63.1, 63.4, 63.3, 63.0, 62.3, 61.3, 60.0, 58.4, 56.4, 54.2, 51.7, 48.9, 45.8, 42.4, 38.8, 35.0, 30.9, 26.8, 22.7, 18.7, 15.0, 11.6, 8.7, 6.5, 4.8, 4.0, 3.8, 4.4, 5.5, 7.0 ])

    norm_offset_ank = 20
    norm_mean_ank = norm_offset_ank + np.array([ -22.3, -22.8, -23.9, -25.2, -26.2, -26.5, -26.1, -25.5, -24.6, -23.6, -22.5, -21.5, -20.5, -19.7, -19.0, -18.3, -17.7, -17.1, -16.6, -16.0, -15.6, -15.1, -14.6, -14.1, -13.6, -13.2, -12.8, -12.4, -12.1, -11.7, -11.4, -11.1, -10.8, -10.4, -10.0, -9.6, -9.3, -8.9, -8.5, -8.2, -8.0, -7.7, -7.6, -7.5, -7.6, -7.7, -7.9, -8.3, -8.9, -9.7, -10.6, -11.9, -13.4, -15.3, -17.6, -20.3, -23.4, -26.6, -29.8, -32.8, -35.3, -37.4, -38.9, -39.8, -39.9, -39.3, -38.0, -36.4, -34.6, -32.6, -30.7, -28.9, -27.3, -25.8, -24.3, -23.0, -21.8, -20.8, -20.0, -19.3, -18.6, -18.1, -17.6, -17.4, -17.3, -17.3, -17.6, -18.1, -18.7, -19.4, -20.0, -20.6, -21.1, -21.5, -21.9, -22.2, -22.5, -22.7, -22.7, -22.5, -22.3 ])
    
    
    try:
        ank = np.rad2deg(df["ankle_angle_l"][step_index[-2]:step_index[-1]])
        kne = np.rad2deg(df["knee_angle_l"][step_index[-2]:step_index[-1]])*(-1)
        hip = np.rad2deg(df["hip_flexion_l"][step_index[-2]:step_index[-1]])
        
        resampled_ank = np.interp(range(len(norm_mean_ank)), range(len(ank)), ank)
        resampled_kne = np.interp(range(len(norm_mean_kne)), range(len(kne)), kne)
        resampled_hip = np.interp(range(len(norm_mean_hip)), range(len(hip)), hip)
        
        R_ank = np.correlate(resampled_ank, norm_mean_ank,'full')
        R_kne = np.correlate(resampled_kne, norm_mean_kne,'full') 
        R_hip = np.correlate(resampled_hip, norm_mean_hip,'full')
        
        # Normalize the cross-correlation similar to xcorr(x,y,'scaleopt') 
        R_ank_norm = np.max(R_ank / np.sqrt(np.dot(resampled_ank, resampled_ank) * np.dot(norm_mean_ank, norm_mean_ank)))
        R_kne_norm = np.max(R_kne / np.sqrt(np.dot(resampled_kne, resampled_kne) * np.dot(norm_mean_kne, norm_mean_kne)))
        R_hip_norm = np.max(R_hip / np.sqrt(np.dot(resampled_hip, resampled_hip) * np.dot(norm_mean_hip, norm_mean_hip)))
        
    except: 
         warnings.warn(
             '''get_cross_corr: Unable to calculate cross correlation for hip, 
             knee and ankle.''', UserWarning
        )
                                                 
    return R_ank_norm, R_kne_norm, R_hip_norm


# -*- coding: utf-8 -*-
def model_execution_SCONE(X: list[float], SCONE_labels: list, Model_labels:list, 
                          model_execution_config: dict):
    """
    Model optimization, evaluation, and post-processing for neuromuscular 
    simulations in SCONE.
    
    The workflow includes:
        1. Generation of SCONE scenarios (.scone files) and Hyfydy models (.hfd files).
        2. Optimization of all scenarios using SCONE Hyfydy.
        3. Evaluation of optimization results.
        4. Postprocessing of simulation output.

   Args:
       X (list[float]): List of input parameters for the optimization.
       SCONE_labels (list): List of labels for SCONE scenarios.
       Model_labels (list): List of labels for Hyfydy models.
       model_execution_config (dict): Configuration dictionary containing flags and file paths.


   Returns:
       Y_headers = ['APO_max',   # Max. ankle power peak
                    'APO_AMP',   # Ankle power amplification: max. power/min. power 
                    'Step_Vel',  # Average forward velocity
                    'E_Metab',   # Total metabolic energy consuption 
                    'E_CoT',     # Cost of transport: E_Metab/m*distance
                    'R_RoM_Ank', # Cross-correlation to human data for ankle
                    'R_RoM_Kne', # Cross-correlation to human data for knee
                    'R_RoM_Hip', # Cross-correlation to human data for hip
                    'Simtime']   # Total simulation time 
        
        out_df_sto (pd.DataFrame): DataFrame with raw simulation output data.
        out_df_res (pd.DataFrame): DataFrame with summarized simulation data.
                 
    Example:
    >>> X = [[1, 2], [4, 5]]
    >>> SCONE_labels = ["gastroc_l { max_isometric_force.factor", 
                        "gastroc_r { max_isometric_force.factor"]
    >>> Y_out = model_execution(X, SCONE_labels)
    
    ---------------------------------------------------------------------------
    
    © Alexandra Buchmann, Chair of Applied Mechanics, alexandra.buchmann@tum.de
    
    ---------------------------------------------------------------------------
    Initiaized:       Mon Apr 24 12:55:07 2023, Munich
    Last modified:    04-01-2024, Munich 
    ---------------------------------------------------------------------------
    
   """
    import os
    import time # Process analysis: evaluate duration of individual modules ---
    start_time = time.time()
    
    #%% 1: Create optimization scenarios (.scone files and .hfd models)
    from scone_batch_workflow import ( 
        generate_scone_files, generate_model_files 
    )
    
    print("\n" + "#" * 20 + " File Generation " + "#" * 23) # -----------------
    
    if model_execution_config['flag_genfiles']:
        # Generate list ascending of numbers for batch command e.g. 00, 01, 02,...
        gen_number_list = lambda N:[f"{i:0{len(str(N))}}" for i in range(1, N+1)]
        num_list = gen_number_list(X.shape[0])
        

        print("Generating SCONE scenarios:") # --------------------------------
        for i in range(X.shape[0]):        
            generate_scone_files(
                model_execution_config['scenario'],
                model_execution_config['optim_dir'],
                model_execution_config['scenario_name'], 
                X[i,:len(SCONE_labels)], 
                num_list[i], 
                SCONE_labels
            )
            
            # ----------------------- Print first and last file name to console 
            if (i == 0) or (i == len(X)-1):
                path = os.path.join(model_execution_config['optim_dir'], model_execution_config['scenario_name'])
                
                new_file_name = f"{path[:-6]}_{num_list[i]}.scone"
                print(f"- {new_file_name}")
                
                if (i==0):
                    print("...")
                                 
                    
        print("\n Generating Hyfydy models:") # ----------------------------------
        
        for i in range(X.shape[0]): 
            generate_model_files(
                model_execution_config['model'],
                model_execution_config['model_dir'], 
                model_execution_config['model_name'],
                X[i,-len(Model_labels):], 
                num_list[i], 
                Model_labels
            )
            
            if (i == 0) or (i == len(X)-1):
                path = os.path.join(model_execution_config['model_dir'], model_execution_config['model_name'])
                
                new_file_name = f"{path[:-4]}_{num_list[i]}.hfd"
                print(f"- {new_file_name}")
                
                if (i==0):
                    print("...")
    
                
    files_time = time.time()
            
    #%% 2: Optimize all scenarios (batch-commands sconecmd.exe -o)
    from scone_batch_workflow import (
        generate_scone_bat_optim, generate_scone_bat_eval, execute_bat_file,
        remove_intermediate_par_files
    )
    
    print("\n" + "#" * 20 + " SCONE Optmization " + "#" * 21) # ---------------
    
    if model_execution_config['flag_optim']:
        batch_size = 10 # Number of optimizations started in parallel --------- 
        generate_scone_bat_optim(model_execution_config['optim_dir'], "scone_optim_all.bat", batch_size)
        execute_bat_file(model_execution_config['optim_dir'], "scone_optim_all.bat")      
 
    optim_time = time.time()
    
    
    print("\n" + "#" * 20 + " SCONE Evaluation " + "#" * 22) # ----------------
    # 3: Evaluate optimization results (batch-command sconecmd.exe -e) 
    if model_execution_config['flag_eval']:
        # 2.3: Trash intermediate optimization results (keep last .par file) --
        remove_intermediate_par_files(model_execution_config['results_dir'])
        
        generate_scone_bat_eval(model_execution_config['results_dir'], "scone_eval_all.bat")
        execute_bat_file(model_execution_config['results_dir'], "scone_eval_all.bat")
        print(f'Finished {num_list[-1]} restults_xx.txt files')
        
        
    eval_time = time.time()
    
    #%% 4: Postprocess simulation output (.par.sto files)
    import numpy as np
    import pandas as pd
    
    from scone_data_postprocess import ( 
        read_sto_files_parallel, read_results_data, 
        get_ankle_details, get_cross_corr
    )
    
    print("\n" + "#" * 20 + " Data Postprocessing " + "#" * 19) # -------------
    
    print('Read in .sto files') # ---------------------------------------------
    out_df_sto = read_sto_files_parallel(model_execution_config['results_dir']) 
    readsto_time = time.time()
    
    
    print('Read in .txt files') # ---------------------------------------------
    out_df_res = read_results_data(model_execution_config['results_dir'], X.shape[0])
    readtxt_time = time.time()

                    
    Y_headers = ['APO_max',   # Max. ankle power peak
                 'APO_AMP',   # Ankle power amplification: max. power/min. power 
                 'Step_Vel',  # Average forward velocity
                 'E_Metab',   # Total metabolic energy consuption 
                 'E_CoT',     # Cost of transport: E_Metab/m*distance
                 'R_RoM_Ank', # Cross-correlation to human data for ankle
                 'R_RoM_Kne', # Cross-correlation to human data for knee
                 'R_RoM_Hip', # Cross-correlation to human data for hip
                 'Simtime']   # Total simulation time  
    
    Y_out = np.zeros([X.shape[0], len(Y_headers)])
    # Y_out[:,:] = np.nan
    
    Ank_det = np.array(list(map(get_ankle_details, out_df_sto)))
    Y_out[:,0] = Ank_det[:,0] #list(map(get_max_ankle_power, out_df_sto))
    Y_out[:,1] = Ank_det[:,1] #list(map(get_p_ank_amp, out_df_sto))
    
    Y_out[:,2] = out_df_res['step_velocity'] 
    Y_out[:,3] = out_df_res['effort'] 
    Y_out[:,4] = np.divide(out_df_res['effort'],(74.5314*out_df_res['distance'])) 
    
    R_all_norm = np.array(list(map(get_cross_corr, out_df_sto)))
    Y_out[:,5] = R_all_norm[:,0]  # Ankle
    Y_out[:,6] = R_all_norm[:,1]  # Knee
    Y_out[:,7] = R_all_norm[:,2]  # Hip
      
    Y_out[:,8] = out_df_res['time'] # Total simulation time until failure
    
    Y_out = pd.DataFrame(Y_out, columns=Y_headers) 
    
    # Remove simulations that fall early - caution, problems with nans so far
    # condition = (out_df_res['time'] != 20)
    # valid_indices = [index for index, value in enumerate(out_df_sto) if condition[index]]
    # Y_out.iloc[valid_indices, :] = np.nan
    
    end_time = time.time()
    
    #%% Evaluate durations of model execution modules
    print("\n" + "#" * 20 + " Process Summary " + "#" * 23) # -----------------
    
    total    = end_time     - start_time
    files    = files_time   - start_time
    optim    = optim_time   - files_time
    evaluate = eval_time    - optim_time
    readsto  = readsto_time - eval_time
    readtxt  = readtxt_time - readsto_time
    calctime = end_time     - readtxt_time
    
    print("Total process time: {:.2f} s".format(total))
    print("File Generation:    {:.2f} s ({:.2f} %)".format(files,   (files/total)*100))
    print("SONE Optimization:  {:.2f} s ({:.2f} %)".format(optim,   (optim/total)*100))
    print("SCONE Evaluation:   {:.2f} s ({:.2f} %)".format(evaluate,(evaluate/total)*100))
    print("Read .sto files:    {:.2f} s ({:.2f} %)".format(readsto, (readsto/total)*100))
    print("Read .txt files:    {:.2f} s ({:.2f} %)".format(readtxt, (readtxt/total)*100))
    print("Data postprocess:   {:.2f} s ({:.2f} %)".format(calctime,(calctime/total)*100))

    return Y_out, out_df_sto, out_df_res
    



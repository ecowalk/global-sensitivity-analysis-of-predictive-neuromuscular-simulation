# Global Sensitivity Analysis of Predictive Neuromuscular Simulation

## Description
Sensitivity analysis (SA) for predictive neuromuscular gait models using Python, [SCONE](https://scone.software/doku.php?id=start) with [Hyfydy](https://hyfydy.com/) [[3]](3) and the [SAFE Toolbox](https://github.com/SAFEtoolbox/SAFE-python) by Pianosi et. al. [[1]](1). 
This repository provides Python modules for a fully integrated workflow to create, optimize and analyze multiple predictive neuromuscular simulation scenarios in parallel. We use the SAFE toolbox to sample the input parameter space and compute sensitivity indices based on model evaluation results.  
    
Supported SA methods (see also SAFE documentation)
- eet (elementary effects test or Morris method)  
- fast (fourier amplitude sensitivity test)  
- pawn (uses cumulative distribution functions (CDFs))  
- vbsa (variance-based sensitivity analysis, or the Sobol' method)  


## Getting Started

1. Install the required software
The following software needs to be installed on your computer:
- [ ] [SCONE](https://scone.software/doku.php?id=start) [[2]](2) with Hyfydy [[3]](3) (Institutional Non-Commercial License required; pricing see [https://hyfydy.com/pricing/](https://hyfydy.com/pricing/))  
- [ ] [Anaconda](https://www.anaconda.com/)  
- [ ] [Git for Windows](https://gitforwindows.org/)  

2. Clone the git repository to your local machine  
Go to [This computer] "Documents - SCONE" and clone the repository into the SCONE folder: right click &rarr; git bash  
```git clone https://gitlab.lrz.de/ecowalk/global-sensitivity-analysis-of-predictive-neuromuscular-simulations.git```

3. Prepare SCONE: open SCONE Studio and  
- adjust the output settings ```Tools - Preferences - Data``` and activate the output of body position and orientation, muscle moments and powers, ground reaction forces  
- enter your Hyfydy license in ```Tools - Preferences - Hyfydy``` 

It is possible to run the code without an active Hyfydy license. You will need to select a different base model (.osim instead of .hyfydy). Note that for large sample sizes, the optimization for each trial will then take significantly longer.

4. Creating an Environment in Anaconda 
Open the Anaconda command prompt (type "anaconda" in Windows Search) and run the following commands.  
```
conda create --name SCONE_SAFE python=3.9 numpy scipy matplotlib numba pandas spyder  
conda activate SCONE_SAFE  
pip install safepython  
spyder  
```

This will install all the required Python packages:  
- [ ] [SAFE Toolbox](https://github.com/SAFEtoolbox/SAFE-python) [[1]](1)  
- [ ] Python version 3.9 with [numpy](https://numpy.org/), [scipy](https://scipy.org/), [matplotlib](https://matplotlib.org/), [numba](https://numba.pydata.org/), [pandas](https://pandas.pydata.org/)  

Once Spyder is up and running, open the ```main.py``` file, press run and follow the instructions on the command line :-)  

## Programm Structure
<img src="Workflow.png" height="250">

### Function workflow in ```main.py```:
1. Define **experimental setup** and **sample input data**: safe_sampling.py - function to sample input parameteres based on the choosen method    
2. **Model execution and evaluation**: ```scone_model_execution.py``` runs multiple SCONE scenarios using [batch optimization from the command line](https://scone.software/doku.php?id=doc:batch)  
	- Delete old files when starting a new optimization with scone_batch_workflow ```delete_files``` and ```clear_folder```  
	- Generate [SCONE scenarios](https://scone.software/doku.php?id=doc:scenario) (.scone files) and [Hyfydy models](https://scone.software/doku.php?id=hyfydy) (.hfd files)  
		- *scone_batch_workflow.py*: ```generate_scone_files()```  and ```generate_model_files()```  
    - [Optimize](https://scone.software/doku.php?id=doc:optimizer) all scenarios using [Hyfydy](https://hyfydy.com/)   
		- *scone_batch_workflow.py*: ```generate_scone_bat_optim()``` and ```start_batch_file()```  
	- Evaluate optimization results  
		- *scone_batch_workflow.py*: ```generate_scone_bat_eval()``` and ```start_batch_file()```  
	- Postprocess simulation output  
		- *scone_batch_workflow.py*: ```remove_intermediate_par_files()```  
		- *scone_data_postprocess.py*: ```read_sto_files_parallel()``` and ```read_results_data()```  
		- *scone_data_postprocess.py*: ```detect_step()```, ```get_ankle_details()```, ```get_cross_corr()```  
3. **Postprocess** model outcome: Perform SA analysis using SAFE and visualize results: ```safe_plotting.py```  
 

#### Modules:
*safe_sampling.py*: provides a function to sample input parameters for sensitivity analysis
```
    - sample_inputs()
```

*scone_batch_workflow.py*: functions to [run SCONE with batch optimization from the command line](https://scone.software/doku.php?id=doc:batch)
```
    - delete_files()  
    - clear_folder()  
    - generate_scone_files()  
    - generate_model_files()  
    - generate_scone_bat_eval()  
    - generate_scone_bat_optim()  
    - start_batch_file()  
    - remove_intermediate_par_files()  
```

*scone_data_postprocess.py*: functions to read in and process SCONE output data [(.sto)](https://scone.software/doku.php?id=doc:sto) and (.txt)
```
    - read_sto_files_parallel()  
    - read_results_data()  
    - detect_step()  
    - get_ankle_details()  
    - get_cross_corr()  
```

*safe_plotting.py*: functions to read in and process SCONE output data [(.sto)](https://scone.software/doku.php?id=doc:sto) and (.txt)
```
    - scatter3d_with_projections()
    - plot_VBSA_indices ()
    - plot_VBSA()
    - plot_EE()
    - plot_FAST_indices()
    - plot_PAWN ()
    - plot_parallel_coordinates() !under developement!
```


## Settings 
### Part 1: Define **experimental setup**
You need to select the parameters you want to subject to SA. This can be any parameter from either model.hfd (e.g. geometric properties of the model, muscle path, masses, inertia...) 
or scenario.scone - see the [SCONE Tutorials](https://scone.software/doku.php?id=tutorials:pathological_gait) for some examples.

You need a base model for both files. New models and scenarios are created based on them by performing a regular expression search for the parameters you define, 
replacing the values after the '=' with the sampled data. Each scenario.scone has its own model.hfd, e.g. scenario_001.scone works with model_001.hfd.

See *scone_batch_workflow.py*: ```generate_scone_files()``` and ```generate_model_files()``` for more information. Sample size and all other parameters should be self-explanatory.  

**Example of what you define in the code**:
```
...
X_Labels = ['GAS', 'SOL', 'HFL'] 

Model_Labels = [
	r'\$calcn_x_pos_GAS',
        r'\$calcn_x_pos_SOL',
        r'\$femur_x_pos_GAS'
]

SCONE_Labels = [
    r'gastroc_.\s*{\s*stiffness_multiplier.factor',
    r'soleus_.\s*{\s*stiffness_multiplier.factor',
    r'iliopsoas_.\s*{\s*stiffness_multiplier.factor'
] 
... 
```

For each new scenario.scone file, the **values** in the file are replaced and the associated model.hfd file is set to the same file number as the created scenario.scone file:
```
# Model used in simulation
ModelHyfydy {
	model_file = ../models/H0914_**file_no**.hfd

	# Override model properties - Multiplier applied to passive tendon an muscle elastic forces 
	Properties {
	 
		gastroc_l { stiffness_multiplier.factor = **value** }
		gastroc_r { stiffness_multiplier.factor = **value** }
		soleus_l { stiffness_multiplier.factor = **value** }
		soleus_r { stiffness_multiplier.factor = **value** }
		iliopsoas_l { stiffness_multiplier.factor = **value** }
		iliopsoas_r { stiffness_multiplier.factor = **value** }
	}
	... 
}
```

Similarly, the corresponding model.hfd file is adapted, where we have defined variables for the parameters to be changed on top of the file: 
```
# Ankle joint (in negative x-direction)
$calcn_x_pos_SOL = **value**
$calcn_x_pos_GAS = **value**

# Knee joint (in negative x-direction)
$femur_x_pos_GAS = **value**

# Hip joint (in negative x-direction)
# $femur_x_pos_HFL = **value**

...
point_path_muscle {
		name = gastroc_r
		tendon_slack_length = 0.39
		optimal_fiber_length = 0.06
		max_isometric_force = 2241
		pennation_angle = 0.296706
		path [
			{ body = femur_r pos { x = $femur_x_pos_GAS y = -0.216 z = -0.024 } }
			{ body = calcn_r pos { x = $calcn_x_pos_GAS y = 0.001 z = -0.0053 } }
		]
	}
....
```

**NOTE**:  You need to make sure that your spelling *exactly* matches the spelling in your model files.
If you want to make the same adjustments for the right and left leg, please also make sure that you formulate your regular expression search so that *property_l* and *property_r* are both adjusted!

### Part 2: Model execution and optimization settings
##### Model execution
All settings for model execution are defined in the dict ```model_execution_config```. The default settings are to run a new optimization whenever you start the script.
```
'flag_genfiles': True,  # Generate new SCONE files from X_all samples
'flag_optim':    True,  # Start optimization for all new files in optim_dir
'flag_eval':     True,  # Evaluate results for all files in results_dir
```

If you have already finished the optimizations and just want to re-run the post-processing on the files in the SCONE results folder, you can set *flag_genfiles* and *flag_optim* to ```False```. 
The files will not be deleted, and you can, for example, try out new data evaluation techniques or plots without re-running the entire code.  

**NOTE**: If you want to *skip* the optimization process, but you do not have any entries in the results folder yet, setting *flag_optim* to ```False``` will not work, as for this framework SCONE needs to run at least one generation in the optimizer to create an entry in the results directory.
Please follow the instructions below to change the optimizer settings if you want to keep the optimization short for debugging reasons. 

##### CMA Optimizer 
You can change the attributes of the optimizer used in SCONE directly in the [batch script](https://scone.software/doku.php?id=doc:batch) used to start the optimizer from the command line. 
All available attributes are listed [here](https://scone.software/doku.php?id=ref:cma_optimizer). 
To speed up the code (e.g. for testing or debugging) you can reduce *CmaOptimizer.max_generations*, to skip the optimization set ```CmaOptimizer.max_generations=1```.

Line 275 in *scone_batch_workflow.py* ```generate_scone_bat_optim()```:
```
...
start "%%f" %SCONECMD% -o "%%f" CmaOptimizer.max_generations=60
...
```

### Part 3: Postprocessing
Step 4 in ```scone_model_execution.py```, line 143ff: You can add or change things here according to your needs. 
If you need more simulation output than what is currently provided in the ```.sto``` files, please adjust SCONE's output settings in ```Tools - Preferences - Data```.

All currently implemented analysis methods are based on the [SAFE Toolbox](https://github.com/SAFEtoolbox/SAFE-python) [[1]](1) followinf best practice from Pianosi et. al. [[5]](5).

If you want to use plots from the SAFE toolbox within subfigures, it is necessary to modify the source code of the toolbox to suppress the creation of a new figure. You have to comment out ```plt.figure()``` in the corresponding modules that are loaded at the beginning of the postprocessing section:
```
import safepython.VBSA as VB 
import safepython.EET  as EET 
import safepython.FAST as FAST 
import safepython.PAWN as PAWN 
```

In addition, you can comment out font family specifications in the toolbox to get the same fonts and sizes for all figures. See [troubleshooting](##Troubleshooting) for more information.

## License
This repository is licensed under the [GNU General Public License 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html). It depends on the following external libraries:
- [SCONE Software](https://scone.software/doku.php?id=license) - [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
- [SAFE Toolbox](https://github.com/SAFEtoolbox/SAFE-python) - [GNU General Public License 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Troubleshooting  
Two errors frequently occur when running the code for the first time after installation.  

#### Problem 1
```
Traceback (most recent call last):

  File c:\users\xxxx\documents\scone\global-sensitivity-analysis-of-predictive-neuromuscular-simulations\main.py:303
    plot_EE(mi, sigma, mi_m, mi_lb, mi_ub, sigma_m, sigma_lb, sigma_ub,

  File ~\Documents\SCONE\global-sensitivity-analysis-of-predictive-neuromuscular-simulations\safe_plotting.py:496 in plot_EE
    ax3.get_legend().remove()

AttributeError: 'NoneType' object has no attribute 'remove'
```
**Exception**: The ```plot_EE``` command starts its own figure when the plot command is called instead of using the axis from the sub-figure we want to create, so we can't access the legend to modify it. 

**Solution**: Comment out ```plt.figure()``` in line 634 of ```EET.py``` from your previously installed SAFE toolbox.


#### Problem 2  
```
Traceback (most recent call last):

  File c:\users\xxxx\documents\scone\global-sensitivity-analysis-of-predictive-neuromuscular-simulations\main.py:345
    KS_median_c_boot, KS_mean_c_boot, KS_max_c_boot = PAWN.pawn_convergence(X_all, Y_all_eval, n, NN, Nboot)

  File ~\anaconda3\envs\SCONE_SAFE_TEST\lib\site-packages\safepython\PAWN.py:742 in pawn_convergence
    Yj = Y[idx_new, :]

IndexError: too many indices for array: array is 1-dimensional, but 2 were indexed
```
**Exception**: The array dimensions used to evaluate the PAWN indices in the SAFE toolbox are not compatible with the current version of numpy.  

**Solution**: In line 742, ```PAWN.py```, change ```Yj = Y[idx_new,:]``` to ```Yj = Y[idx_new]#,:]``` to match the array dimensions we need for our analysis.

## Support
If you have any problems, please contact alexandra.buchmann@tum.de. I'll be happy to help!

## References:
[1]: Pianosi, Francesca; Sarrazin, Fanny; Wagener, Thorsten (2015): A Matlab toolbox for Global Sensitivity Analysis. In: Environmental Modelling & Software 70, S. 80–85. DOI: 10.1016/j.envsoft.2015.04.009   
[2]: Geijtenbeek, Thomas (2019): SCONE. Open Source Software for Predictive Simulation of Biological Motion. In: JOSS 4 (38), S. 1421. DOI: 10.21105/joss.01421   
[3]: Geijtenbeek, Thomas (2021): The Hyfydy Simulation Software. Online verfügbar unter https://hyfydy.com  
[4]: Geyer, Hartmut; Herr, Hugh (2010): A muscle-reflex model that encodes principles of legged mechanics produces human walking dynamics and muscle activities. In: IEEE Transactions on Neural Systems and Rehabilitation Engineering : a Publication of the IEEE Engineering in Medicine and Biology Society 18 (3), S. 263–273. DOI: 10.1109/TNSRE.2010.2047592   
[5]: Pianosi, Francesca; Beven, Keith; Freer, Jim; Hall, Jim W.; Rougier, Jonathan; Stephenson, David B.; Wagener, Thorsten (2016): Sensitivity analysis of environmental models: A systematic review with practical workflow. In: Environmental Modelling & Software 79, S. 214–232. DOI: 10.1016/j.envsoft.2016.02.008   
